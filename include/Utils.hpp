#pragma once
#include <SFML/Graphics.hpp>

class Utils
{
public:

	static uint32_t wavelength2ARGB(float wavelength);

	static sf::Vector2f flipX(const sf::Vector2f& vec);

	static sf::Vector2f flipY(const sf::Vector2f& vec);

	static float angle(const sf::Vector2f& a, const sf::Vector2f& b);

	static float length(const sf::Vector2f& vec);

	static float rad2deg(float rad);

	static float deg2rad(float deg);

	template<typename T>
	static T clamp(const T& val, const T& lo, const T& hi)
	{
		if(val < lo) return lo;
		if(val > hi) return hi;
		return val;
	}

	static sf::Color interpolate(const sf::Color& begin, const sf::Color& end, float amount);

	static sf::Vector2f direction(float fAngle);

};
