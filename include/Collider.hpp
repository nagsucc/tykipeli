#pragma once
#include <SFML/Graphics.hpp>

class Collider
{
public:

	Collider();
	~Collider();

	enum class Type
	{
		None,
		Point,
		Rect,
		Custom
	} colliderType;

	union Bounds
	{
		sf::Vector2f point;
		sf::FloatRect rect;

		Bounds()
		{
			rect = sf::FloatRect(0.0f, 0.0f, 0.0f, 0.0f);
		}

	} colliderBounds;

	float colliderRadius = 1.0f;

	void checkCollision(Collider* other);
	
	bool collides(Collider* other);
	
	virtual bool rayHit(const sf::Vector2f& pointA, const sf::Vector2f& pointB, float fResolution = 1.0f);

	virtual bool checkCustomCollision(Collider* other);
};
