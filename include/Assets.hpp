#pragma once
#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class Assets
{
public:

	Assets();
	~Assets();

	bool loadTexture(const std::string& filename, const std::string& id = "");
	bool loadFont(const std::string& filename, const std::string& id = "");
	bool loadSound(const std::string& filename, const std::string& id = "");
	bool loadFileAsString(const std::string& filename, const std::string& id = "");

	void releaseTexture(const std::string& id);
	void releaseFont(const std::string& id);
	void releaseSound(const std::string& id);
	void releaseString(const std::string& id);

	sf::Texture* getTexture(const std::string& id);
	sf::Font* getFont(const std::string& id);
	sf::SoundBuffer* getSound(const std::string& id);
	std::string getString(const std::string& id);

private:

	std::string parseFilename(const std::string& filename);

	std::map<std::string, sf::Texture> m_Textures;
	std::map<std::string, sf::Font> m_Fonts;
	std::map<std::string, sf::SoundBuffer> m_Sounds;
	std::map<std::string, std::string> m_Strings;

};
