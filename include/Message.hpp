#pragma once

#include <cstring>
#include <SFML/Graphics.hpp>

class GameObject;
class Listener;
class Collider;

struct CollisionData
{
	Collider* colliderA = nullptr;
	Collider* colliderB = nullptr;
	sf::Vector2f hitPosition = {0, 0};
	sf::Vector2f forceVector = {0, 0};
};

struct ExplosionData
{
	sf::Vector2f position = {0, 0};
	float radius = 0.0f;
	GameObject* parent = nullptr;
	float damage = 0.0f;
};

struct SpawnData
{
	uint32_t id;
	GameObject* object;
};

struct DamageData
{
	GameObject* from;
	GameObject* to;
	float amount;
};

struct DestroyData
{
	GameObject* object;
};

struct AimData
{
	GameObject* cannon;
	sf::Vector2f position;
};

struct ShootData
{
	GameObject* cannon;
};

struct Message
{
	// Listener* target = nullptr;
	// Listener* sender = nullptr;

	enum class Id
	{
		Undefined = 0,
		Event,
		Spawn,
		Aim,
		Explosion,
		Collision,
		Damage,
		Destroyed,
		Shoot,
		Exit,
		Count
	} id;

	static const char* IdName[(int)Id::Count];

	// enum class Type
	// {
	// 	NoData = 0,
	// 	Event,
	// 	ExplosionData,
	// 	CollisionData,
	// 	SpawnData,
	// 	DamageData,
	// 	DestroyData,
	// 	Count
	// } type;

	union Data
	{
		sf::Event event;
		ExplosionData explosion;
		CollisionData collision;
		SpawnData spawn;
		DamageData damage;
		DestroyData destroyed;
		AimData aim;
		ShootData shoot;

		Data()
		{
			memset(this, 0, sizeof(Data));
		}
	} data;
};
