#pragma once
#include <iostream>
#include <string>
#include <mutex>

#include "Listener.hpp"

class Log : public Listener
{
public:

	Log();
	~Log();

	void setOutputStream(std::ostream* pStream);
	void useConsoleColors(bool bEnabled);
	void setSuppressMessages(bool bSuppress, Message::Id id = Message::Id::Count);
	void receive(const Message& message);

	void info(const std::string& message);
	void note(const std::string& message);
	void success(const std::string& message);
	void warning(const std::string& message);
	void error(const std::string& message);

	enum class Color
	{
		Default, Gray, Red, Blue, Green, Yellow
	};

private:

	std::ostream* m_streamOut = &std::cout;
	std::mutex m_mutex;

	bool m_bUseConsoleColors = true;
	bool m_bSuppressMessages = false;
	bool m_bSuppressMessageIds[(int)Message::Id::Count] = { 0 };

	Color m_ColorInfo = Color::Gray;
	Color m_ColorNote = Color::Blue;
	Color m_ColorSuccess = Color::Green;
	Color m_ColorWarning = Color::Yellow;
	Color m_ColorError = Color::Red;

	void setColor_(Color color, std::string& message);
	void log_(const std::string& message);

};
