#pragma once
#include "GUI/Control.hpp"

class Panel : public Control
{
public:

	Panel();
	virtual ~Panel();
	virtual void init();
	virtual void uninit();
	virtual void update(const float& deltaTime);
	virtual void apply();

protected:

	virtual void setColor(const sf::Color& background, const sf::Color& foreground);

	virtual void onEnable_();

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::RectangleShape m_Background;
};
