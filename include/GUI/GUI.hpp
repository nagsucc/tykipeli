#pragma once
#include <memory>
#include <map>
#include <SFML/Graphics.hpp>
#include "GUI/Control.hpp"
#include "Input.hpp"

class GUI : public sf::Drawable
{
public:

	GUI();
	virtual ~GUI();
	
	void setInputBindings(Input* pInputSystem);
	void update(const float& deltaTime);

	void setVisible(bool bVisible);
	bool getVisible() const;

	template<typename T>
	uint32_t createControl()
	{
		std::shared_ptr<Control> pControl = std::make_shared<T>();
		pControl->m_Id = m_iControlId;
		pControl->m_pParent = this;
		m_Controls[m_iControlId] = pControl;
		m_iControlId++;
		return pControl->m_Id;
	}

	void destroyControl(uint32_t id);

	template<typename T = Control>
	std::shared_ptr<T> get(uint32_t id)
	{
		auto it = m_Controls.find(id);
		if(it != m_Controls.end())
		{
			return std::dynamic_pointer_cast<T>(it->second);
		}
		return nullptr;
	}
	
	void onMouseMove(sf::Vector2i pos);
	void onMouseDown(sf::Mouse::Button button);
	void onMouseUp(sf::Mouse::Button button);
	void onMouseScroll(sf::Mouse::Wheel wheel, int delta);
	void onTextEntered(uint32_t unicode);

	struct Colors
	{
		sf::Color Background = sf::Color(0x9BBDF9FF);
		sf::Color Foreground = sf::Color(0x121212FF);

		sf::Color BackgroundDisabled = sf::Color::Transparent;
		sf::Color ForegroundDisabled = sf::Color(0xB95F89FF);

		sf::Color BackgroundHover = sf::Color(0x67AAF9FF);
		sf::Color ForegroundHover = sf::Color(0x121212FF);

		sf::Color BackgroundActive = sf::Color(0x2EC0F9FF);
		sf::Color ForegroundActive = sf::Color(0xC4E0F9FF);
	};

	void setColors(const Colors& colors);
	const Colors& getColors() const;

	void setFont(sf::Font* pFont);
	sf::Font* getFont() const;

	void setBorderWidth(float fWidth);
	float getBorderWidth() const;

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	Input* m_pInput = nullptr;
	bool m_bVisible = true;

	uint32_t m_iControlId = 0;
	std::map<uint32_t, std::shared_ptr<Control>> m_Controls;

	Colors m_Colors;
	sf::Font* m_pFont = nullptr;
	float m_fBorderWidth = 2.0f;
};
