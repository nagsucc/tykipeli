#pragma once
#include <SFML/Graphics.hpp>

class GUI;

class Control : public sf::Drawable, public sf::IntRect
{
public:

	Control();
	virtual ~Control();
	virtual void init();
	virtual void uninit();
	virtual void update(const float& deltaTime);

	// Apply changes made to position and size
	virtual void apply();

	uint32_t getId() const;

	void setVisibile(bool bVisible);
	bool isVisible() const;

	void setEnabled(bool bEnabled);
	bool isEnabled() const;

	bool isDragged() const;
	bool isHovering() const;
	bool isActive() const;
	
protected:

	friend class GUI;

	GUI* m_pParent = nullptr;
	uint32_t m_Id = (uint32_t) -1;

	bool m_bHover = false;
	bool m_bDrag = false;
	bool m_bEnabled = false;
	bool m_bActive = false;

	virtual void onEnter_() {}
	virtual void onExit_() {}
	virtual void onClick_(sf::Mouse::Button button) {}
	virtual void onDragBegin_() {}
	virtual void onDrag_(const sf::Vector2i& pos) {}
	virtual void onDragEnd_() {}
	virtual void onEnable_() {}
	virtual void onDisable_() {}
	virtual void onScroll_(sf::Mouse::Wheel wheel, int delta) {}
	virtual void onTextEntered(uint32_t unicode) {}

	enum class Transition
	{
		None,
		Enable,
		Hover,
		Active
	};

	enum class TransitionDirection
	{
		Forward,
		Backwards
	};

	Transition m_Transition = Transition::None;
	TransitionDirection m_TransitionDirection = TransitionDirection::Forward;

	void beginTransition(Transition transition, TransitionDirection direction);
	void updateTransition(const float& deltaTime);

	float m_fTransitionTimer = 0.0f;
	const float m_fTransitionDuration = 0.2f;

	virtual void setColor(const sf::Color& background, const sf::Color& foreground) {}

private:

	bool m_bVisible = true;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const {}

};
