#pragma once
#include <functional>
#include "GUI/Control.hpp"

class CheckBox : public Control
{
public:

	CheckBox();
	virtual ~CheckBox();
	virtual void init();
	virtual void uninit();
	virtual void update(const float& deltaTime);
	virtual void apply();

	bool isChecked() const;
	void setChecked(bool bChecked);
	void check();
	void uncheck();

	std::function<void(bool)> onValueChanged;
	void setText(const std::string& text);


protected:

	virtual void onEnter_();
	virtual void onExit_();
	virtual void onClick_(sf::Mouse::Button button);
	virtual void onDragBegin_();
	virtual void onEnable_();
	virtual void onDisable_();

	virtual void setColor(const sf::Color& background, const sf::Color& foreground);

	sf::RectangleShape m_Box;

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Text m_Label;
	sf::RectangleShape m_Mark;

	bool m_bChecked = false;
};
