#pragma once
#include <functional>
#include "GUI/Control.hpp"

class Button : public Control
{
public:

	Button();
	virtual ~Button();
	virtual void init();
	virtual void uninit();
	virtual void update(const float& deltaTime);
	virtual void apply();

	std::function<void()> onClick;
	void setText(const std::string& text);

protected:

	virtual void onEnter_();
	virtual void onExit_();
	virtual void onClick_(sf::Mouse::Button button);
	virtual void onDragBegin_();
	virtual void onEnable_();
	virtual void onDisable_();

	virtual void setColor(const sf::Color& background, const sf::Color& foreground);

private:

	sf::Text m_Label;
	sf::RectangleShape m_Background;
	sf::RectangleShape m_Border;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};
