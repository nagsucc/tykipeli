#pragma once
#include "GUI/Control.hpp"


class Label : public Control
{
public:

	Label();
	virtual ~Label();
	virtual void init();
	virtual void uninit();
	virtual void update(const float& deltaTime);
	virtual void apply();

	void setText(const std::string& text);

	enum class Align
	{
		Left,
		Center,
		Right
	};

	void setAlign(Align align);
	void setInvertColors(bool bEnable);

protected:

	virtual void setColor(const sf::Color& background, const sf::Color& foreground);

	virtual void onEnable_();

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Text m_Text;
	bool m_bInvertColors = false;
	Align m_Align = Align::Left;
};
