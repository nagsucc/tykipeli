#pragma once
#include "GUI/CheckBox.hpp"

class ColoredCheckBox : public CheckBox
{
public:

	ColoredCheckBox();
	virtual ~ColoredCheckBox();

	void setBoxColor(const sf::Color& color);

protected:

	virtual void setColor(const sf::Color& background, const sf::Color& foreground);

private:

	sf::Color m_Color;
};
