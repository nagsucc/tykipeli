#pragma once
#include <SFML/Graphics.hpp>
#include "Collider.hpp"

class Scene;

class GameObject : public sf::Drawable, public sf::Transformable, public Collider
{
public:

	GameObject();
	virtual ~GameObject();

	uint32_t getId() const;
	Scene* getScene() const;

	void setActive(bool bActive);
	bool isActive() const;

	virtual void update(const float& deltaTime) = 0;

	void setVelocity(const sf::Vector2f& velocity);
	const sf::Vector2f& getVelocity() const;

	void setMass(float fMass);
	float getMass() const;

	void enablePhysics(bool bEnable);
	bool isPhysicsEnabled() const;

private:

	friend class Scene;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

	bool m_bActive = true;
	Scene* m_pScene = nullptr;
	uint32_t m_Id = (uint32_t) -1;

	sf::Vector2f m_velocity = {0, 0};
	float m_fMass = 1.0f;
	bool m_bIgnorePhysics = true;

};
