#pragma once
#include <mutex>
#include "Message.hpp"

class Listener;

class MessageBus
{
public:

	MessageBus();
	~MessageBus();

	void registerListener(Listener* pListener);
	void unregisterListener(Listener* pListener);
	void send(const Message& message);

private:

	std::mutex m_Mtx;
	std::vector<Listener*> m_Listeners;

};
