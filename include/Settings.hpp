#pragma once
#include <SFML/Graphics.hpp>

namespace Settings
{
	extern int PlayerCount;
	extern int PlayerNumber;

	static const int MinPlayers = 4;
	static const int MaxPlayers = 6;

	static const sf::Color BaseColors[MaxPlayers] = 
	{
		sf::Color(0xBB1111FF), // Red
		sf::Color(0xBBBB11FF), // Yellow
		sf::Color(0x00FF00FF), // Green
		sf::Color(0x1111BBFF), // Blue
		sf::Color(0xBB11BBFF), // Magenta
		sf::Color(0xFF8410FF) // Orange
	};

	// static const char* BaseNames[MaxPlayers] = 
	// {
	// 	"Red",
	// 	"Yellow",
	// 	"Green",
	// 	"Blue",
	// 	"Purple",
	// 	"Orange"
	// };
}
