#pragma once
#include <vector>
#include <memory>
#include "GameObject.hpp"

class Physics
{
public:

	Physics();
	~Physics();

	void setGravity(float fGravity);
	float getGravity() const;

	void setUnit(float fPixels);
	float getUnit() const;

	template<typename T>
	T unit2pixels(T value)
	{
		return static_cast<T>(value * m_fUnitInPixels);
	}

	void update(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects);

private:

	void collisions(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects);
	void gravity(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects);
	void apply(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects);

	float m_fGravity = 9.81f;
	float m_fUnitInPixels = 20.0f;
};
