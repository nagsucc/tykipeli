#pragma once

#include <string>

#include "SceneManager.hpp"
#include "Timer.hpp"
#include "MessageBus.hpp"
#include "Renderer.hpp"
#include "Log.hpp"
#include "Physics.hpp"
#include "Assets.hpp"
#include "Input.hpp"

#include "Objects/Cursor.hpp"

class Game
{
public:

	Game();
	~Game();

	static SceneManager sceneManager;
	static Timer timer;
	static MessageBus messageBus;
	static Renderer renderer;
	static Log log;
	static Physics physics;
	static Assets assets;
	static Input input;
	static Cursor cursor;

	static void init();
	static void uninit();
	static void run();
	static void exit();

	static bool bExitRequested;

	static std::string sTitle;
	static std::string sIcon;
	static std::string sVersion;
};
