#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"

class Cannon : public GameObject, public Listener
{
public:

	enum class State
	{
		Idle,
		Destroyed,
		Aiming
	};

	Cannon();
	virtual ~Cannon();

	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);

	void aimAt(const sf::Vector2i& position);
	void shoot();

	void setHealth(float fHealth);
	float getHealth() const;

	void setColor(sf::Color color);

	State getState() const;
	void setState(State eState);

	sf::Vector2f getBarrelPos() const;

	static const float fMaxAimLength;

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::RectangleShape m_Base;
	sf::RectangleShape m_Barrel;
	sf::RectangleShape m_AimLine;
	sf::RectangleShape m_HealthBarBg;
	sf::RectangleShape m_HealthBar;

	float m_fHealth = 100.0f;

	sf::Vector2f m_vTarget = {0,0};
	float m_fPower = 0.0f;

	sf::Color m_BaseColor = sf::Color::White;
	State m_State = State::Idle;
};
