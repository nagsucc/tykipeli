#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"

class Projectile : public GameObject, public Listener
{
public:

	Projectile();
	virtual ~Projectile();

	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);

	virtual void launch(const sf::Vector2f& direction, float fPower);
	virtual void onCollision();

	void setRadius(float fRadius);
	
	void setDamage(float fDamage);
	float getDamage() const;

	GameObject* parent = nullptr;

	static const float fMaxLaunchVelocity;
	static const float fMinLaunchVelocity;

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::CircleShape m_Projectile;
	
	float m_fDamage = 25.0f;
	float m_fRadius = 5.0f;

};
