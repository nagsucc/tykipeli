#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"

class LoadIcon : public GameObject, public Listener
{
public:

	LoadIcon();
	virtual ~LoadIcon();

	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::RectangleShape m_Spinner;

};
