#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"

class Collider;

class Terrain : public GameObject, public Listener
{
public:

	Terrain();
	virtual ~Terrain();

	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);

	void create(uint32_t iWidth, uint32_t iHeight);
	void setColor(sf::Color color);
	void setZeroLevel(float fRelativeZero);

	void onExplosion(const ExplosionData& explosion);

	bool checkCustomCollision(Collider* other);

	sf::FloatRect getBounds() const;

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Image m_Terrain;
	sf::Texture m_Texture;
	sf::Sprite m_Sprite;

	sf::Color m_Color =  sf::Color(0x009900FF);
	float m_ZeroLevel = 0.4f;

};
