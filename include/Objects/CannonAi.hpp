#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"
#include <map>
#include "Objects/Cannon.hpp"
#include "Objects/Terrain.hpp"

class CannonAi : public GameObject, public Listener
{
public:

	CannonAi();
	virtual ~CannonAi();
	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);
	virtual void init();

	struct TargetStats
	{
		uint32_t hitsTaken;
		float health;
		bool directLine;
	};

	enum class Status
	{
		Disabled,
		Processing,
		Ready
	};

	Status getStatus() const;
	void setStatus(Status status);

	void attachTo(Cannon* pCannon);
	void setTerrain(Terrain* pTerrain);

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const {}

	void updateTargetStats();
	uint32_t pickTarget();
	sf::Vector2f calculateAim(uint32_t targets);

	//float m_fTolerance = 10.0f;
	int m_iMaxVarianceAngle = 5.0f;
	int m_iMaxVariancePower = 5.0f;

	Cannon* m_pCannonSelf = nullptr;
	Terrain* m_pTerrain = nullptr;
	std::map<uint32_t, TargetStats> m_Targets;

	Status m_Status = Status::Ready;
};
