#pragma once
#include "GameObject.hpp"
#include "Listener.hpp"

class Cursor : public GameObject, public Listener
{
public:

	Cursor();
	virtual ~Cursor();
	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);

	void setTexture(sf::Texture& texture);

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Sprite m_Sprite;

};
