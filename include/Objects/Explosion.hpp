#pragma once
#include "GameObject.hpp"

class Explosion : public GameObject
{
public:

	Explosion();
	virtual ~Explosion();

	virtual void update(const float& deltaTime);

	void explode();
	void onExploded();
	void setExplosionRadius(float fRadius);
	void setExplosionTime(float fSeconds);
	bool hasExploded() const;
	void setDamage(float fDamage);
	float getDamage() const;

	GameObject* parent;
	
private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::CircleShape m_Explosion;

	bool m_bExploding = false;
	
	float m_fDamage = 0.0f;
	float m_fExplosionRadius = 75.0f;
	float m_fExplosionTime = 2.0f;
	float m_fCurrentTime = 0.0f;
	float m_fRelativeTime = 0.0f;
};
