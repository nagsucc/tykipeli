#pragma once
#include <memory>
#include <unordered_map>
#include <typeinfo>
#include <vector>
#include "GameObject.hpp"
#include "Listener.hpp"
#include "SceneManager.hpp"

class Renderer;

class Scene : public Listener
{
public:

	Scene(SceneId id);
	virtual ~Scene();
	virtual void onEnter();
	virtual void onExit();
	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);
	virtual void draw(Renderer* pRenderer);

	SceneId getId() const;

	bool isActive() const;
	void setActive(bool bActive);

	template<typename T>
	uint32_t spawnObject()
	{
		std::shared_ptr<GameObject> pObj = std::make_shared<T>();

		pObj->m_pScene = this;
		pObj->m_Id = m_iObjectId;
		pObj->m_bActive = true;
		
		m_Objects[m_iObjectId] = pObj;
		m_iObjectId++;
		m_ObjectPointers.push_back(pObj);

		return pObj->m_Id;
	}

	void destroyObject(uint32_t iObjectId);

	template<typename T = GameObject>
	std::shared_ptr<T> getObject(uint32_t iObjectId)
	{
		auto it = m_Objects.find(iObjectId);
		return std::dynamic_pointer_cast<T>(it->second);
	}

	std::vector<std::shared_ptr<GameObject>> getObjects() const;

	template<typename T = GameObject>
	std::vector<std::shared_ptr<T>> getAll()
	{
		std::vector<std::shared_ptr<T>> objects;
		for(uint32_t i = 0; i < m_ObjectPointers.size(); i++)
		{
			if(auto p = std::dynamic_pointer_cast<T>(m_ObjectPointers[i]))
			{
				objects.push_back(p);
			}
		}
		return objects;
	}

private:

	bool m_bActive = false;
	SceneId m_Id = SceneId::None;

	uint32_t m_iObjectId = 0;
	std::unordered_map<uint32_t, std::shared_ptr<GameObject>> m_Objects;
	std::vector<std::shared_ptr<GameObject>> m_ObjectPointers;

};
