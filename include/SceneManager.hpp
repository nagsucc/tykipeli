#pragma once
#include <stack>
#include <memory>

#include "Listener.hpp"

enum class SceneId
{
	None = -1,
	MainMenu = 0,
	Battle
};

#include "Scene.hpp"

class SceneManager : public Listener
{
public:

	SceneManager();
	~SceneManager();

	void update(const float& deltaTime);
	void receive(const Message& message);

	void pushScene(SceneId id);
	void popScene();
	void swapScene(SceneId id);
	std::shared_ptr<Scene> getCurrentScene() const;
	void resetScene();

private:

	std::stack<std::shared_ptr<Scene>> m_SceneStack;
	SceneId m_CurrentScene = SceneId::None;

};
