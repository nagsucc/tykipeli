#pragma once
#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Listener.hpp"

class Renderer : public Listener
{
public:

	Renderer();
	~Renderer();

	void init();
	void uninit();

	void update(const float& deltaTime);
	void receive(const Message& message);

	void beginDraw();

	template<typename T>
	void draw(T arrayDrawables[], uint32_t size)
	{
		for(uint32_t i = 0; i < size; i++)
		{
			m_pWindow->draw(arrayDrawables[i]);
		}
	}

	void draw(const std::vector<sf::Drawable*>& drawables);
	void draw(const sf::Drawable& drawable);
	void endDraw();

	std::weak_ptr<sf::RenderWindow> getWindow() const;

	sf::Vector2u getScreenSize() const;
	void setScreenSize(const sf::Vector2u& vSize);

	uint32_t getFramelimit() const;
	void setFramelimit(uint32_t iLimit);

	sf::Color getWindowColor() const;
	void setWindowColor(sf::Color color);

	void setShowCursor(bool bShow);

private:

	std::shared_ptr<sf::RenderWindow> m_pWindow;
	sf::VideoMode m_VideoMode = sf::VideoMode(1600, 900);

	bool m_bShowCursor = true;
	uint32_t m_iFrameLimit = 60;
	uint32_t m_iStyle = sf::Style::Close;
	sf::Color m_WindowColor = sf::Color::Black;
};
