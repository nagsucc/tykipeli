#pragma once
#include <SFML/System.hpp>

#define TIMER_FPS_SAMPLE_COUNT 50

class Timer
{
public:

	Timer();
	~Timer();

	void restart();

	unsigned int getFps() const;
	unsigned int getSmoothFps() const;
	float getFrametime() const;
	float getDeltaTime() const;

private:

	float m_fDelta;
	sf::Clock m_Clock;

	float m_FrameTimes[TIMER_FPS_SAMPLE_COUNT] = { 0 };
	uint32_t m_iSampleIndex = 0;
	uint32_t m_iFps = 0;
};