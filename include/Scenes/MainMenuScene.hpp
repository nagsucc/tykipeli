#pragma once
#include "Scene.hpp"
#include "GUI/GUI.hpp"
#include "Settings.hpp"

class MainMenuScene : public Scene
{
public:

	MainMenuScene();
	virtual ~MainMenuScene();
	virtual void onEnter();
	virtual void onExit();
	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);
	virtual void draw(Renderer* pRenderer);

private:

	uint32_t m_LoadIcon;
	GUI m_Gui;
	uint32_t m_ColorToggles[Settings::MaxPlayers];
	uint32_t m_OpponentCountToggles[Settings::MaxPlayers - Settings::MinPlayers + 1];

	void onBeginClicked();
	void onExitClicked();
	void onSelectColor(uint32_t index);
	void onSelectPlayerCount(uint32_t count);
	
};