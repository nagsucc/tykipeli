#pragma once
#include "Scene.hpp"
#include "GUI/GUI.hpp"
#include "Settings.hpp"

class BattleScene : public Scene
{
public:

	BattleScene();
	virtual ~BattleScene();
	virtual void onEnter();
	virtual void onExit();
	virtual void update(const float& deltaTime);
	virtual void receive(const Message& message);
	virtual void draw(Renderer* pRenderer);

private:

	enum class State
	{
		Init,
		RoundBegin,
		PlayerAim,
		ProcessAI,
		ShootProjectiles,
		RoundOver,

		Count
	};

	const char* StateStatus[(int)State::Count] =
	{
		"Initializing...",
		"The game will begin in ",
		"Ready to fire",
		"Processing AI...",
		"Reloading...",
		"Game over! Returning to main menu in "
	};

	uint32_t m_HudStateLabel;
	// uint32_t m_HudBattleLog;
	// uint32_t m_HudCenterLabel;

	State m_State = State::Init;
	float m_fTimer = 0.0f;

	const float m_fRoundBeginDelay = 6.0f;
	const float m_fRoundOverDelay = 10.0f;
	const float m_fAiProcessDelay = 0.5f;

	void setState(State state);

	uint32_t m_Terrain;
	uint32_t m_Player;

	GUI m_Hud;
	GUI m_PauseMenu;

	uint32_t m_Players[Settings::MaxPlayers] = { (uint32_t) -1 };
	uint32_t m_CannonAi[Settings::MaxPlayers] = { (uint32_t) -1 };
	int getNumberOfPlayersAlive();

	void onKeyHit(sf::Keyboard::Key key);
	void onMouseMove(sf::Vector2i pos);
	void onMouseUp(sf::Mouse::Button button);
	void forceMouseUpdate();

	void onContinueClicked();
	void onRetryClicked();
	void onExitClicked();
	void togglePause();

	void setInputBindings();
	void setAimCursor(bool bAim);

	void spawnPlayers(uint32_t iCount);
	void spawnTerrain();
	void initPauseMenu();
	void initHud();
};
