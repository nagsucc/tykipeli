#pragma once
#include "Message.hpp"

class Listener
{
public:

	Listener(const std::string& name = "");
	~Listener();

	virtual void receive(const Message& message) = 0;
	const std::string& getName() const;

private:

	std::string m_sName;

};
