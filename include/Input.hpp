#pragma once
#include <functional>
#include <SFML/Window.hpp>
#include "Listener.hpp"

class Input : public Listener
{
public:

	Input();
	virtual ~Input();

	void receive(const Message& message);

	void resetBindings();

	template<typename T>
	inline void resetBinding(std::function<T>& funcRef)
	{
		funcRef = nullptr;
	}

	std::function<void(sf::Keyboard::Key key)> onKeyDown;
	std::function<void(sf::Keyboard::Key key)> onKeyUp;
	std::function<void(sf::Mouse::Button button)> onMouseDown;
	std::function<void(sf::Mouse::Button button)> onMouseUp;
	std::function<void(sf::Vector2i vPosition)> onMouseMove;
	std::function<void(sf::Mouse::Wheel wheel, int delta)> onMouseScroll;
	std::function<void(unsigned int controllerId)> onControllerConnected;
	std::function<void(unsigned int controllerId)> onControllerDisconnected;
	std::function<void(sf::Joystick::Axis axis, unsigned int controllerId, float value)> onControllerMove;
	std::function<void(unsigned int controllerId, unsigned int button)> onControllerButtonDown;
	std::function<void(unsigned int controllerId, unsigned int button)> onControllerButtonUp;
	std::function<void(uint32_t iUnicode)> onTextEntered;

	// virtual void onKeyDown(sf::Keyboard::Key key) {}
	// virtual void onKeyUp(sf::Keyboard::Key key) {}
	// virtual void onMouseDown(sf::Mouse::Button button) {}
	// virtual void onMouseUp(sf::Mouse::Button button) {}
	// virtual void onMouseMove(sf::Vector2f vPosition) {}
	// virtual void onMouseScroll(sf::Mouse::Wheel wheel, int delta) {}
	// virtual void onControllerConnected(unsigned int controllerId) {}
	// virtual void onControllerDisconnected(unsigned int controllerId) {}
	// virtual void onControllerMove(sf::Joystick::Axis axis, unsigned int controllerId, float value) {}
	// virtual void onControllerButtonDown(unsigned int controllerId, unsigned int button) {}
	// virtual void onControllerButtonUp(unsigned int controllerId, unsigned int button) {}
	// virtual void onTextEntered(uint32_t iUnicode) {}

	static bool isMouseDown(sf::Mouse::Button);
	static bool isKeyDown(sf::Keyboard::Key key);
	static bool isControllerButtonDown(unsigned int controllerId, unsigned int button);
	static bool isControllerConnected(unsigned int controllerId);

	void setTextInputMode(bool bEnable);
	bool getTextInputMode() const;

private:

	bool m_bTextInput = false;

};
