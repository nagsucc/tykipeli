# Tykipeli

This is the repository for a small 2D battle game called Tykipeli ("The Cannon Game"). It was born from a programming assignment part of my studies at South-Eastern Finland University of Applied Sciences. 

The game is being worked on in quite a tight schedule since I do not have much spare time. This results in naïve design choices regarding some aspects, such as memory management, as I have to implement a minimum set of features in somewhat reasonable timespan. 

In the big picture, my intention is to construct an easily expandable base of a game engine which I could utilize in my future projects.

Status: _Minimal features_

# Dependencies

This project uses [SFML](https://www.sfml-dev.org).
The recommended way of acquiring the SFML libraries is through a package management system.

* On Linux distributions, the package name may vary. On Manjaro Linux (which I use), one should run `sudo pacman -S sfml`.
* On macOS, I'd recommend you to use [Homebrew](https://brew.sh): `brew install sfml`.
* On Windows, use the package management tool included with MSYS2. See more details below.

# Building

Windows
-------
* Install and set up [MSYS2](https://www.msys2.org) (x64).
* Install MinGW toolchain, SFML, make and git.

```
pacman -S --needed base-devel mingw-w64-x86_64-toolchain mingw-w64-x86_64-sfml git make
```

* Follow the instructions for Linux & macOS in MSYS2 MINGW command prompt.

Linux & macOS
-------------
* Clone the repo: `git clone https://gitlab.com/nagsucc/tykipeli.git`.
* Move into the working directory: `cd tykipeli`.
* Compile the code: `make debug` or `make release`.
* Run the game: `./bin/debug/Tykipeli` or `./bin/release/Tykipeli`.

# TODO
* Ammo types: number of projectiles, special behaviour
* Threading
* In-game console
* Entity-Component-System
* Sprites
* Audio effects
* Physics: Wind
* JSON: Localizations, asset manifests and configs
* GameObjects: only private methods, communication explicitly via message bus?
* Custom renderer (Vulkan/OpenGL)