#include "Game.hpp"

MessageBus Game::messageBus;
SceneManager Game::sceneManager;
Timer Game::timer;
Renderer Game::renderer;
Log Game::log;
Physics Game::physics;
Assets Game::assets;
Input Game::input;
Cursor Game::cursor;

bool Game::bExitRequested = false;
std::string Game::sTitle = "Game";
std::string Game::sIcon = "";
std::string Game::sVersion = "1.0";

Game::Game()
{

}

Game::~Game()
{

}

void Game::init()
{
	log.info("Initializing " + Game::sTitle + " v." + Game::sVersion + "...");

	assets.loadTexture("res/cursor_aim.png");
	assets.loadTexture("res/icon.png");
	assets.loadFont("res/OpenSans-Regular.ttf", "default");

	renderer.init();
	log.note("Init ok");

	cursor.setActive(true);
}

void Game::uninit()
{

}

void Game::run()
{
	std::weak_ptr<sf::RenderWindow> pWindow = renderer.getWindow();
	timer.restart();

	while(bExitRequested == false)
	{
		if(auto window = pWindow.lock())
		{
			if(window->isOpen() == false)
			{
				exit();
			}

			sf::Event event;
			while(window->pollEvent(event))
			{
				Message msg;
				{
					msg.id = Message::Id::Event;
					msg.data.event = event;
				}
				messageBus.send(msg);
			}

			sceneManager.update(timer.getDeltaTime());
			cursor.update(timer.getDeltaTime());

			renderer.beginDraw();

			// Draw objects here
			auto pScene = sceneManager.getCurrentScene();
			if(pScene)
			{
				pScene->draw(&renderer);
			}

			renderer.draw(cursor);
			renderer.endDraw();

			renderer.update(timer.getDeltaTime());
			timer.restart();

		}
	}
}

void Game::exit()
{
	bExitRequested = true;
}