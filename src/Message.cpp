#include "Message.hpp"

const char* Message::IdName[(int)Id::Count] = 
{
	"Undefined",
	"Event",
	"Spawn",
	"Aim",
	"Explosion",
	"Collision",
	"Damage",
	"Destroyed",
	"Shoot",
	"Exit"
};