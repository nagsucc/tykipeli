#include "Collider.hpp"
#include "Utils.hpp"
#include "Message.hpp"
#include "Game.hpp"

Collider::Collider()
{

}

Collider::~Collider()
{

}

void Collider::checkCollision(Collider* other)
{
	if(collides(other))
	{
		CollisionData data;
		data.colliderA = this;
		data.colliderB = other;

		Message msg;
		msg.id = Message::Id::Collision;
		msg.data.collision = data;
		Game::messageBus.send(msg);
	}
}

bool Collider::collides(Collider* other)
{
	bool bCollision = false;

	if(colliderType == Type::Custom && other->colliderType != Type::Custom)
	{
		bCollision = checkCustomCollision(other);
	}
	else if (colliderType != Type::Custom && other->colliderType == Type::Custom)
	{
		bCollision = other->checkCustomCollision(this);
	}
	else if(colliderType == Type::Point)
	{
		if(other->colliderType == Type::Point)
		{
			float distance = Utils::length(colliderBounds.point - other->colliderBounds.point);
			bCollision = distance < (colliderRadius + other->colliderRadius);
		}
		else if (other->colliderType == Type::Rect)
		{
			bCollision = other->colliderBounds.rect.contains(colliderBounds.point);
		}
	}
	else if (colliderType == Type::Rect)
	{
		if(other->colliderType == Type::Point)
		{
			bCollision = colliderBounds.rect.contains(other->colliderBounds.point);
		}
		else if (other->colliderType == Type::Rect)
		{
			bCollision = colliderBounds.rect.intersects(other->colliderBounds.rect);
		}
	}

	return bCollision;
}

bool Collider::rayHit(const sf::Vector2f& pointA, const sf::Vector2f& pointB, float fResolution)
{
	Collider point;
	point.colliderType = Type::Point;
	point.colliderRadius = fResolution;
	
	if(colliderType == Type::None)
	{
		return false;
	}
	
	sf::Vector2f v = pointB - pointA;
	float l = Utils::length(v);
	sf::Vector2f dir = v / l;

	for(float delta = 0; delta <= l; delta += fResolution)
	{
		point.colliderBounds.point = pointA + dir * delta;
		bool hit = false;

		if(colliderType == Type::Point)
		{
			float distance = Utils::length(colliderBounds.point - point.colliderBounds.point);
			hit = distance < (colliderRadius + point.colliderRadius);
		}
		else if (colliderType == Type::Rect)
		{
			hit = colliderBounds.rect.contains(point.colliderBounds.point);
		}
		else if (colliderType == Type::Custom)
		{
			hit = checkCustomCollision(&point);
		}

		if(hit) return true;
	}

	return false;
}

bool Collider::checkCustomCollision(Collider* other)
{
	return false;
}