#include "Objects/Cannon.hpp"
#include "Game.hpp"
#include "Utils.hpp"
#include "Scene.hpp"
#include "Objects/Projectile.hpp"

const float Cannon::fMaxAimLength = 400.0f;

Cannon::Cannon()
{
	m_Base.setFillColor(sf::Color::White);
	m_Base.setSize({50.0f, 40.0f});
	m_Base.setOrigin({25.0f, 40.0f});
	
	m_Barrel.setFillColor(sf::Color::Red);
	m_Barrel.setSize({50.0f, 8.0f});
	m_Barrel.setOrigin({0.0f, 4.0f});

	m_AimLine.setFillColor(sf::Color::Blue);
	m_AimLine.setSize({fMaxAimLength, 1.0f});
	m_AimLine.setOrigin({0.0f, 0.5f});

	m_HealthBarBg.setFillColor(sf::Color(0x121212CC));
	m_HealthBarBg.setSize({40.0f, 10.0f});
	m_HealthBarBg.setOrigin({20.0f, 10.0f});

	m_HealthBar.setFillColor(sf::Color::Green);
	m_HealthBar.setSize({36.0f, 6.0f});
	m_HealthBar.setOrigin({-2.0f, 8.0f});
	
	colliderType = Collider::Type::Rect;
	this->enablePhysics(true);
}

Cannon::~Cannon()
{

}

void Cannon::update(const float& deltaTime)
{
	sf::Vector2f pos = this->getPosition();
	m_Base.setPosition(pos);
	pos.y -= 35.f;
	m_Barrel.setPosition(pos);
	m_AimLine.setPosition(pos);

	colliderBounds.rect = m_Base.getGlobalBounds();

	sf::Color color = m_BaseColor;
	if(m_fHealth <= 0.0f)
	{
		color = Utils::interpolate(color, sf::Color::Black, 0.5f);
	}
	m_Base.setFillColor(color);

	pos.y -= 20.0f;
	m_HealthBarBg.setPosition(pos);

	pos.x -= 20.0f;
	m_HealthBar.setPosition(pos);

	float health = m_fHealth / 100.0f;
	m_HealthBar.setSize({36.0f * health, 6.0f});

	sf::Color healthColor = sf::Color::Green;
	if(health < 0.2)
	{
		healthColor = sf::Color::Red;
	}
	else if(health < 0.6)
	{
		healthColor = sf::Color::Yellow;
	}
	m_HealthBar.setFillColor(healthColor);

}

void Cannon::receive(const Message& message)
{
	if(message.id == Message::Id::Aim)
	{
		if(message.data.aim.cannon == this)
		{
			this->aimAt(sf::Vector2i(message.data.aim.position));
		}
		return;
	}

	if(message.id == Message::Id::Shoot)
	{
		if(message.data.shoot.cannon == this)
		{
			this->shoot();
		}
		return;
	}

	if(message.id == Message::Id::Collision)
	{
		if(message.data.collision.colliderA == this ||
			message.data.collision.colliderB == this)
		{
			sf::Vector2f vel = this->getVelocity();
			vel.y = 0.0f;
			this->setVelocity(vel);
		}

		return;
	}

	if(message.id == Message::Id::Explosion)
	{
		const ExplosionData& explosion = message.data.explosion;

		sf::FloatRect rect = m_Base.getGlobalBounds();
		sf::Vector2f center = sf::Vector2f(rect.left + rect.width / 2.0f, rect.top + rect.height / 2.0f);

		rect.left -= explosion.radius;
		rect.top -= explosion.radius;
		rect.width += explosion.radius * 2.0f;
		rect.height += explosion.radius * 2.0f;

		if(rect.contains(explosion.position))
		{
			float distance = Utils::length(explosion.position - center);

			float radiusSqLen = Utils::length(sf::Vector2f(
				explosion.radius, explosion.radius));
				
			float damage = explosion.damage * (radiusSqLen - distance) / radiusSqLen;
			if(damage < 0.0f) damage = 0.0f;

			DamageData data;
			data.from = explosion.parent;
			data.to = this;
			data.amount = damage;

			Message damageMessage;
			damageMessage.id = Message::Id::Damage;
			damageMessage.data.damage = data;
			Game::messageBus.send(damageMessage);
		}

		return;
	}

	if(message.id == Message::Id::Damage)
	{
		if(message.data.damage.to == this)
		{
			m_fHealth = m_fHealth - message.data.damage.amount;
			if(m_fHealth <= 0.0f)
			{
				m_fHealth = 0.0f;
				m_State = State::Destroyed;

				DestroyData data;
				data.object = this;

				Message destroyedMessage;
				destroyedMessage.id = Message::Id::Destroyed;
				destroyedMessage.data.destroyed = data;
				Game::messageBus.send(destroyedMessage);
			}
		}
		return;
	}

}

void Cannon::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isActive() == false) 
		return;

	target.draw(m_Base, states);

	if(m_fHealth > 0.0f)
	{
		if(m_State == State::Aiming)
		{
			target.draw(m_AimLine, states);
		}
		
		target.draw(m_Barrel, states);
	}

	target.draw(m_HealthBarBg, states);
	target.draw(m_HealthBar, states);
}

void Cannon::aimAt(const sf::Vector2i& position)
{
	sf::Vector2f pos = sf::Vector2f(position);
	sf::Vector2f p = m_Barrel.getPosition();

	float angle_rad = Utils::angle(pos, p);
	float angle = Utils::rad2deg(angle_rad);

	angle = angle < 90.0f ? angle + 360.0f : angle;
	angle = Utils::clamp(angle, 180.0f, 360.0f);

	m_Barrel.setRotation(angle);
	m_AimLine.setRotation(angle);

	m_vTarget = pos;
	m_fPower = Utils::clamp(Utils::length(p - pos), 0.0f, fMaxAimLength) / fMaxAimLength;

	// Game::log.info("[Cannon " + std::to_string(getId()) + "] Set power level to " + std::to_string(m_fPower * 100.0f) + " %");
	// Game::log.info("[Cannon " + std::to_string(getId()) + "] Set angle to " + std::to_string(angle) + " deg");

	m_AimLine.setSize({fMaxAimLength * m_fPower, 1.0f});
}

void Cannon::shoot()
{
	uint32_t objId = this->getScene()->spawnObject<Projectile>();

	sf::Vector2f pos = m_Barrel.getPosition() + Utils::direction(m_Barrel.getRotation()) * 50.0f;

	Projectile* pProjectile = this->getScene()->getObject<Projectile>(objId).get();
	pProjectile->parent = this;
	pProjectile->setPosition(pos);
	pProjectile->launch(pos - m_Barrel.getPosition(), m_fPower);
}

void Cannon::setHealth(float fHealth)
{
	m_fHealth = fHealth;
}

float Cannon::getHealth() const
{
	return m_fHealth;
}

void Cannon::setColor(sf::Color color)
{
	m_BaseColor = color;
}

Cannon::State Cannon::getState() const
{
	return m_State;
}

void Cannon::setState(Cannon::State eState)
{
	m_State = eState;
}

sf::Vector2f Cannon::getBarrelPos() const
{
	return m_Barrel.getPosition();
}