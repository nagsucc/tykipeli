#include "Objects/Terrain.hpp"
#include <cmath>
#include "Utils.hpp"

Terrain::Terrain()
{
	colliderType = Collider::Type::Custom;
}

Terrain::~Terrain()
{

}

void Terrain::update(const float& deltaTime)
{

}

void Terrain::receive(const Message& message)
{
	if(message.id == Message::Id::Explosion)
	{
		onExplosion(message.data.explosion);
	}
}

void Terrain::create(uint32_t iWidth, uint32_t iHeight)
{
	m_Terrain.create(iWidth, iHeight, sf::Color(0x00000000));

	float polarity1 = static_cast<float>(rand() % 3 - 1);
	float amplitude1 = static_cast<float>((rand() % 50 + 50) / 100.0f);
	float phase1 = static_cast<float>(rand() % 100 / 100.0f * M_PI);

	float polarity2 = static_cast<float>(rand() % 3 - 1);
	float amplitude2 = static_cast<float>((rand() % 50 + 50) / 100.0f);
	float phase2 = static_cast<float>(rand() % 100 / 100.0f * M_PI);

	float polarity3 = static_cast<float>(rand() % 3 - 1);
	float amplitude3 = static_cast<float>((rand() % 50 + 50) / 100.0f);
	float phase3 = static_cast<float>(rand() % 100 / 100.0f * M_PI);

	float multiplier4 = static_cast<float>(rand() % 16 + 1);
	float polarity4 = static_cast<float>(rand() % 3 - 1);
	float amplitude4 = static_cast<float>((rand() % 50 + 50) / 100.0f);
	float phase4 = static_cast<float>(rand() % 100 / 100.0f * M_PI);

	for(uint32_t x = 0; x < iWidth; x++)
	{
		for(uint32_t y = 0; y < iHeight; y++)
		{
			float fZero = 1.0f - m_ZeroLevel;
			float fRelX = x / static_cast<float>(iWidth);


			float factor1 = std::sin(fRelX * M_PI + phase1) / 8.0f * polarity1 * amplitude1;
			float factor2 = std::sin(fRelX * M_PI * 2.0f + phase2) / 16.0f * polarity2 * amplitude2;
			float factor3 = std::sin(fRelX * M_PI * 4.0f + phase3) / 14.0f * polarity3 * amplitude3;
			float factor4 = std::cos(fRelX * M_PI * multiplier4 + phase4) / 12.0f * polarity4 * amplitude4;

			uint32_t ground = static_cast<uint32_t>(
				(fZero + factor1 + factor2 + factor3 + factor4) * iHeight
			);
				
			if(ground < y)
			{
				m_Terrain.setPixel(x, y, m_Color);
			}
			
		}
	}

	m_Texture.loadFromImage(m_Terrain);
	m_Sprite.setTexture(m_Texture, true);
}

void Terrain::setColor(sf::Color color)
{
	m_Color = color;
}

void Terrain::setZeroLevel(float fRelativeZero)
{
	m_ZeroLevel = fRelativeZero;
}

void Terrain::onExplosion(const ExplosionData& explosion)
{
	const sf::Vector2u& terrainSize = m_Terrain.getSize();
	int iLeft = static_cast<int>(explosion.position.x - explosion.radius);
	int iRight = static_cast<int>(explosion.position.x + explosion.radius);
	int iTop = static_cast<int>(explosion.position.y - explosion.radius);
	int iBottom = static_cast<int>(explosion.position.y + explosion.radius);

	for(int x = iLeft; x < iRight; x++)
	{
		for(int y = iTop; y < iBottom; y++)
		{
			if(x < 0 || y < 0 || x >= (int)terrainSize.x || y >= (int)terrainSize.y)
				continue;

			float d = Utils::length(explosion.position - sf::Vector2f(x,y));

			if(d <= explosion.radius)
			{
				m_Terrain.setPixel((uint32_t) x, (uint32_t) y, sf::Color::Transparent);
			}
		}
	}

	m_Texture.loadFromImage(m_Terrain);
}

bool Terrain::checkCustomCollision(Collider* other)
{
	if(other->colliderType == Collider::Type::Point)
	{
		int x = static_cast<int>(other->colliderBounds.point.x);
		int y = static_cast<int>(other->colliderBounds.point.y);

		const sf::Vector2u& terrainSize = m_Terrain.getSize();

		if(x < 0 || x >= (int)terrainSize.x || y < 0)
			return false;

		if(y >= (int)terrainSize.y)
			return true;

		if(m_Terrain.getPixel((uint32_t)x, (uint32_t)y) != sf::Color::Transparent)
			return true;
	}
	else if (other->colliderType == Collider::Type::Rect)
	{
		int iLeft = static_cast<int>(other->colliderBounds.rect.left);
		int iRight = iLeft + static_cast<int>(other->colliderBounds.rect.width);
		int iTop = static_cast<int>(other->colliderBounds.rect.top);
		int iBottom = iTop + static_cast<int>(other->colliderBounds.rect.height);

		for(int x = iLeft; x < iRight; x++)
		{
			for(int y = iTop; y < iBottom; y++)
			{
				const sf::Vector2u& terrainSize = m_Terrain.getSize();

				if(x < 0 || x >= (int)terrainSize.x || y < 0)
					continue;

				if(y >= (int)terrainSize.y)
					return true;

				if(m_Terrain.getPixel((uint32_t)x, (uint32_t)y) != sf::Color::Transparent)
					return true;
			}
		}
	}

	return false;
}

sf::FloatRect Terrain::getBounds() const
{
	return m_Sprite.getGlobalBounds();
}

void Terrain::draw(sf::RenderTarget& target, sf::RenderStates states) const 
{
	if(this->isActive())
		target.draw(m_Sprite, states);
}