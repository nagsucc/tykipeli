#include "Objects/CannonAi.hpp"
#include "Objects/Projectile.hpp"
#include "Scene.hpp"
#include "Utils.hpp"
#include "Game.hpp"

CannonAi::CannonAi()
{

}

CannonAi::~CannonAi()
{

}

void CannonAi::update(const float& deltaTime)
{
	if(m_Status == Status::Processing)
	{
		updateTargetStats();

		Game::log.note("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Processing...");

		uint32_t target = pickTarget();
		sf::Vector2f aim = calculateAim(target);

		AimData data;
		data.cannon = m_pCannonSelf;
		data.position = aim;

		Message message;
		message.id = Message::Id::Aim;
		message.data.aim = data;
		Game::messageBus.send(message);

		Game::log.success("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Ready");
		m_Status = Status::Ready;
	}
}

void CannonAi::receive(const Message& message)
{
	if(message.id == Message::Id::Damage)
	{
		if(message.data.damage.to == m_pCannonSelf)
		{
			uint32_t targetId = message.data.damage.from->getId();

			auto it = m_Targets.find(targetId);
			if(it != m_Targets.end())
			{
				it->second.hitsTaken++;
			}
		}
		return;
	}

	if(message.id == Message::Id::Destroyed)
	{
		if(message.data.destroyed.object == m_pCannonSelf)
		{
			setStatus(Status::Disabled);
		}
		else
		{
			uint32_t targetId = message.data.damage.from->getId();

			auto it = m_Targets.find(targetId);
			if(it != m_Targets.end())
			{
				m_Targets.erase(it);
			}
		}
		return;
	}
}

void CannonAi::init()
{
	// Get targets and construct stats
	uint32_t idSelf = m_pCannonSelf->getId();
	auto cannons = getScene()->getAll<Cannon>();

	for(auto& cannon : cannons)
	{
		uint32_t idCannon = cannon->getId();
		if(idCannon == idSelf)
		{
			continue;
		}

		TargetStats stats;
		{
			stats.hitsTaken = 0;
			stats.health = cannon->getHealth();
			stats.directLine = false;
		}
		m_Targets[idCannon] = stats;
	}
}

void CannonAi::updateTargetStats()
{
	for(auto& stats : m_Targets)
	{
		auto pTarget = getScene()->getObject<Cannon>(stats.first);

		if(m_pTerrain != nullptr)
		{
			// Check if direct line of sight exists
			stats.second.directLine = !m_pTerrain->rayHit(m_pCannonSelf->getPosition(), pTarget->getPosition());
		}

		stats.second.health = pTarget->getHealth();
	}
}

uint32_t CannonAi::pickTarget()
{
	uint32_t target = m_Targets.begin()->first;

	// Get the player who has hit me the most
	uint32_t targetMostHits = target;
	uint32_t hitCount = 0;

	for(auto& target : m_Targets)
	{
		if(target.second.hitsTaken > hitCount)
		{
			hitCount = target.second.hitsTaken;
			targetMostHits = target.first;
		}
	}

	// Get the target who's closest
	uint32_t targetClosest = target;
	float closestDistance = 9000001.0f;
	const sf::Vector2f& positionSelf = this->m_pCannonSelf->getPosition();

	for(auto& target : m_Targets)
	{
		const sf::Vector2f& positionTarget = getScene()->getObject<Cannon>(target.first)->getPosition();
		float distance = Utils::length(positionSelf - positionTarget);

		if(distance < closestDistance)
		{
			distance = closestDistance;
			targetClosest = target.first;
		}
	}

	// Get the weakest and strongest target
	uint32_t targetWeakest = target;
	uint32_t targetStrongest = target;

	float lowestHealth = 100.0f;
	float highestHealth = -1.0f;
	float averageHealth = this->m_pCannonSelf->getHealth();

	for(auto& target : m_Targets)
	{
		averageHealth += target.second.health;

		if(target.second.health < lowestHealth)
		{
			lowestHealth = target.second.health;
			targetWeakest = target.first;
		}

		if(target.second.health > highestHealth)
		{
			highestHealth = target.second.health;
			targetStrongest = target.first;
		}
	}

	averageHealth /= (m_Targets.size() + 1);

	// Wildcard
	bool bRandom = (rand() % 100) < 20; // 20% chance for random target
	uint32_t randomIndex = rand() % m_Targets.size();
	uint32_t targetRandom = target;

	uint32_t i = 0;
	for(auto& target : m_Targets)
	{
		if(i == randomIndex)
		{
			targetRandom = target.first;
			break;
		}
		i++;
	}

	// Pick a target
	if(averageHealth * 2.0f < highestHealth) // Time to unite against the strongest
	{
		Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Target the strongest player");
		target = targetStrongest;
	}
	else if(hitCount > 2) // Do we have a nemesis?
	{
		Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Target the nemesis!");
		target = targetMostHits;
	}
	else if (bRandom)
	{
		Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Random target");
		target = targetRandom;
	}
	else if (lowestHealth < 20.0f) // Easy picks
	{
		Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Target weakest");
		target = targetWeakest;
	}
	else // Guess we'll just shoot the neighbour
	{
		Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Target closest");
		target = targetClosest;
	}

	Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] Targeting " + std::to_string(target));

	return target;
}

sf::Vector2f CannonAi::calculateAim(uint32_t target)
{
	sf::Vector2f vTargetPosition = getScene()->getObject<Cannon>(target)->getPosition();
	sf::Vector2f vSelfPosition = m_pCannonSelf->getBarrelPos();

	// Find angle
	float fBestAngle = 270.0f;
	float fBestPower = 0.0f;
	float fBestDistance = 900000.0f;

	const float fMaxTime = 12.0f;
	const float fUnit = Game::physics.getUnit();
	const float fGravity = Game::physics.getGravity();

	Collider coll;
	coll.colliderType = Collider::Type::Point;
	coll.colliderRadius = 10.0f;

	for(float fAngle = 180.0f; fAngle <= 360.0f; fAngle += 1.0f)
	{
		for(float fPower = 0.0f; fPower <= 1.0f; fPower += 0.05f)
		{
			for(float t = 0; t <= fMaxTime; t += 0.05f)
			{
				float pwr = Projectile::fMinLaunchVelocity + (Projectile::fMaxLaunchVelocity - Projectile::fMinLaunchVelocity) * fPower;

				sf::Vector2f pos = vSelfPosition;

				sf::Vector2f delta = pwr * Utils::direction(fAngle) * t;
				delta.y += 0.5f * fGravity * t * t;
				delta *= fUnit;

				pos += delta;
				coll.colliderBounds.point = pos;

				// Has the projectile collided with terrain?
				if(m_pTerrain->collides(&coll))
				{
					float distanceToTarget = Utils::length(pos - vTargetPosition);
					if(distanceToTarget < fBestDistance)
					{
						// These are the best parameters so far
						fBestAngle = fAngle;
						fBestPower = fPower;
						fBestDistance = distanceToTarget;
					}
				}
			}
		}
	}

	fBestAngle += static_cast<float>(rand() % (m_iMaxVarianceAngle * 2) - m_iMaxVarianceAngle);
	fBestPower += static_cast<float>(rand() % (m_iMaxVariancePower * 2) - m_iMaxVariancePower) / 100.0f;

	Game::log.info("[AI " + std::to_string(m_pCannonSelf->getId()) + "] \
		Angle: " + std::to_string(fBestAngle) + " deg\
		Power: " + std::to_string(fBestPower * 100.0f) + " %\n\
		Predicted distance: " + std::to_string(fBestDistance));

	sf::Vector2f dir = vSelfPosition + Utils::direction(fBestAngle) * fBestPower * Cannon::fMaxAimLength;
	return dir;
}

CannonAi::Status CannonAi::getStatus() const
{
	return m_Status;
}

void CannonAi::setStatus(CannonAi::Status status)
{
	m_Status = status;
}

void CannonAi::attachTo(Cannon* pCannon)
{
	m_pCannonSelf = pCannon;
}

void CannonAi::setTerrain(Terrain* pTerrain)
{
	m_pTerrain = pTerrain;
}