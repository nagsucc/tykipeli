#include "Objects/Projectile.hpp"
#include "Utils.hpp"
#include "Scene.hpp"
#include "Objects/Explosion.hpp"

const float Projectile::fMaxLaunchVelocity = 40.0f;
const float Projectile::fMinLaunchVelocity = 4.0f;

Projectile::Projectile()
{
	colliderType = Collider::Type::Point;
	setRadius(m_fRadius);
	this->enablePhysics(true);
}

Projectile::~Projectile()
{

}

void Projectile::update(const float& deltaTime)
{
	if(this->isActive())
	{
		const sf::Vector2f& position = this->getPosition();
		this->colliderBounds.point = position;
	}
}

void Projectile::receive(const Message& message)
{
	if(message.id == Message::Id::Collision)
	{
		if(message.data.collision.colliderA == this ||
			message.data.collision.colliderB == this)
		{
			onCollision();
		}
	}
}

void Projectile::launch(const sf::Vector2f& direction, float fPower)
{
	sf::Vector2f dir = direction / Utils::length(direction);
	float vel = fMinLaunchVelocity + (fMaxLaunchVelocity - fMinLaunchVelocity) * Utils::clamp(fPower, 0.0f, 1.0f);

	this->setVelocity(dir * vel);
	this->setActive(true);
}

void Projectile::onCollision()
{
	uint32_t explosion = this->getScene()->spawnObject<Explosion>();
	auto pExplosion = this->getScene()->getObject<Explosion>(explosion);

	pExplosion->parent = this->parent;
	pExplosion->setPosition(this->getPosition());
	pExplosion->setDamage(m_fDamage);
	pExplosion->explode();

	this->getScene()->destroyObject(this->getId());
}

void Projectile::setRadius(float fRadius)
{
	m_fRadius = fRadius;
	m_Projectile.setRadius(fRadius);
	m_Projectile.setOrigin(fRadius, fRadius);
	colliderRadius = m_fRadius;
}

void Projectile::setDamage(float fDamage)
{
	m_fDamage = fDamage;
}

float Projectile::getDamage() const
{
	return m_fDamage;
}

void Projectile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isActive())
	{
		target.draw(m_Projectile, this->getTransform());
	}
}
