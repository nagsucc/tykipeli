#include "Objects/Explosion.hpp"
#include "Message.hpp"
#include "Utils.hpp"
#include "Game.hpp"

Explosion::Explosion()
{

}

Explosion::~Explosion()
{

}

void Explosion::update(const float& deltaTime)
{
	if(this->isActive() == false)
		return;

	if(m_bExploding)
	{
		m_fCurrentTime += deltaTime;
		m_fRelativeTime = m_fCurrentTime / m_fExplosionTime;

		if(m_fRelativeTime >= 1.0f)
		{
			m_bExploding = false;
			m_fRelativeTime = 1.0f;
			onExploded();
		}

		float radius = m_fExplosionRadius * m_fRelativeTime;

		sf::Color color = Utils::interpolate(sf::Color::White, sf::Color::Red, m_fRelativeTime);

		m_Explosion.setRadius(radius);
		m_Explosion.setOrigin(radius, radius);
		m_Explosion.setPosition(this->getPosition());
		m_Explosion.setFillColor(color);
	}
}

void Explosion::explode()
{
	m_fCurrentTime = 0.0f;
	m_fRelativeTime = 0.0f;
	m_bExploding = true;
}

void Explosion::onExploded()
{
	ExplosionData data;
	data.parent = this->parent;
	data.position = this->getPosition();
	data.radius = this->m_fExplosionRadius;
	data.damage = this->m_fDamage;

	Message message;
	message.id = Message::Id::Explosion;
	message.data.explosion = data;
	Game::messageBus.send(message);

	this->getScene()->destroyObject(this->getId());
}

void Explosion::setExplosionRadius(float fRadius)
{
	m_fExplosionRadius = fRadius;
}

void Explosion::setExplosionTime(float fSeconds)
{
	m_fExplosionTime = fSeconds;
}

bool Explosion::hasExploded() const
{
	return m_fRelativeTime >= 1.0f;
}

void Explosion::setDamage(float fDamage)
{
	m_fDamage = fDamage;
}

float Explosion::getDamage() const
{
	return m_fDamage;
}

void Explosion::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isActive() && m_bExploding)
	{
		target.draw(m_Explosion, states);
	}
}
