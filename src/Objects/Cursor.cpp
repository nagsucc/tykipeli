#include "Objects/Cursor.hpp"

Cursor::Cursor()
{

}

Cursor::~Cursor()
{

}

void Cursor::update(const float& deltaTime)
{
}

void Cursor::receive(const Message& message)
{
	if(message.id == Message::Id::Event)
	{
		const sf::Event& event = message.data.event;
		if(event.type == sf::Event::MouseMoved)
		{
			sf::Vector2f pos = {
				static_cast<float>(event.mouseMove.x),
				static_cast<float>(event.mouseMove.y)
			};

			this->setPosition(pos);
		}
	}
}

void Cursor::setTexture(sf::Texture& texture)
{
	const sf::Vector2u size = texture.getSize();
	m_Sprite.setTexture(texture);
	m_Sprite.setOrigin(sf::Vector2f(size.x / 2.0f, size.y / 2.0f));
}

void Cursor::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isActive())
		target.draw(m_Sprite, this->getTransform());
}