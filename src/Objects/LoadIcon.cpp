#include "Objects/LoadIcon.hpp"

LoadIcon::LoadIcon()
{
	m_Spinner = sf::RectangleShape({200, 200});
	this->setOrigin({100,100});
}

LoadIcon::~LoadIcon()
{

}

void LoadIcon::update(const float& deltaTime)
{
	if(this->isActive())
	{
		this->rotate(45.0f * deltaTime);
	}
}

void LoadIcon::receive(const Message& message)
{
}

void LoadIcon::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isActive())
	{
		target.draw(m_Spinner, this->getTransform());
	}
}
