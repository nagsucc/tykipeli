#include "SceneManager.hpp"
#include "Scenes/MainMenuScene.hpp"
#include "Scenes/BattleScene.hpp"

SceneManager::SceneManager()
{

}

SceneManager::~SceneManager()
{
	while(m_SceneStack.empty() == false)
	{
		m_SceneStack.pop();
	}
}

void SceneManager::update(const float& deltaTime)
{
	if(m_SceneStack.empty() == false)
	{
		std::shared_ptr<Scene> pScene = m_SceneStack.top();
		pScene->update(deltaTime);
	}
}

void SceneManager::receive(const Message& message)
{

}

void SceneManager::pushScene(SceneId id)
{
	if(m_SceneStack.empty() == false)
	{
		m_SceneStack.top()->setActive(false);
		m_SceneStack.top()->onExit();
	}

	m_CurrentScene = id;
	std::shared_ptr<Scene> pScene = nullptr;
	switch(id)
	{
		default:
		case SceneId::None: 
			return;

		case SceneId::MainMenu:
			pScene = std::make_shared<MainMenuScene>();
			break;

		case SceneId::Battle:
			pScene = std::make_shared<BattleScene>();
			break;
	}

	m_SceneStack.push(pScene);
	m_SceneStack.top()->onEnter();
	m_SceneStack.top()->setActive(true);
}

void SceneManager::popScene()
{
	if(m_SceneStack.empty())
		return;

	m_CurrentScene = SceneId::None;
	m_SceneStack.top()->setActive(false);
	m_SceneStack.top()->onExit();
	m_SceneStack.pop();

	if(m_SceneStack.empty() == false)
	{
		m_CurrentScene = m_SceneStack.top()->getId();
		m_SceneStack.top()->onEnter();
		m_SceneStack.top()->setActive(true);
	}
}

std::shared_ptr<Scene> SceneManager::getCurrentScene() const
{
	if(m_SceneStack.empty())
	{
		return nullptr;
	}
	return m_SceneStack.top();
}

void SceneManager::swapScene(SceneId id)
{
	popScene();
	pushScene(id);
}

void SceneManager::resetScene()
{
	swapScene(m_CurrentScene);
}