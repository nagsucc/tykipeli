#include "Timer.hpp"
#include <cmath>

Timer::Timer()
{

}

Timer::~Timer()
{

}

void Timer::restart()
{
	m_fDelta = m_Clock.restart().asSeconds();

	m_iSampleIndex = (m_iSampleIndex + 1) % TIMER_FPS_SAMPLE_COUNT;
	m_FrameTimes[m_iSampleIndex] = m_fDelta;

	if(m_iSampleIndex == 0)
	{
		float sum = 0.0f;
		for(float dt : m_FrameTimes)
		{
			sum += dt;
		}

		float avg = sum / TIMER_FPS_SAMPLE_COUNT;
		m_iFps = static_cast<uint32_t>(std::round(1.0f / avg));
	}

}

uint32_t Timer::getFps() const
{
	if(m_fDelta <= 0) return 0;
	return static_cast<uint32_t>(std::round(1.0f / m_fDelta));
}

uint32_t Timer::getSmoothFps() const
{
	return m_iFps;
}

float Timer::getFrametime() const
{
	return m_fDelta * 1000.0f;
}

float Timer::getDeltaTime() const
{
	return m_fDelta;
}