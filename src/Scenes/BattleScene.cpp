#include "Scenes/BattleScene.hpp"
#include <cmath>
#include "Game.hpp"
#include "Objects/Terrain.hpp"
#include "Objects/Cannon.hpp"
#include "Objects/Projectile.hpp"
#include "Objects/CannonAi.hpp"
#include "GUI/Button.hpp"
#include "GUI/Panel.hpp"
#include "GUI/Label.hpp"

/* PALETTE
#fdc010	(253,192,16) yellow
#8bc34a	(139,195,74) light green
#43a047	(67,160,71) dark green
#009688	(0,150,136) dark blue
#09bcd3	(9,188,211) light blue
*/

BattleScene::BattleScene() : Scene(SceneId::Battle)
{
	for(uint32_t i = 0; i < Settings::MaxPlayers; i++)
	{
		m_Players[i] = (uint32_t) -1;
		m_CannonAi[i] = (uint32_t) -1;
	}

	spawnTerrain();
	spawnPlayers(Settings::PlayerCount);
	initHud();
	initPauseMenu();

	setState(State::RoundBegin);
}

BattleScene::~BattleScene()
{

}

void BattleScene::onEnter()
{
	// Set renderer settings
	Game::renderer.setWindowColor(sf::Color(0x09BCD3FF));

	// Set input bindings
	setInputBindings();

	// Set cursor
	setAimCursor(true);
}

void BattleScene::onExit()
{
	Game::renderer.setShowCursor(true);
	Game::input.resetBindings();
	Game::cursor.setActive(false);
}

void BattleScene::update(const float& deltaTime)
{
	if(m_PauseMenu.getVisible())
	{
		m_PauseMenu.update(deltaTime);
		return;
	}

	// Update state
	if(m_State == State::Init)
	{
		return;
	}
	else if (m_State == State::RoundBegin)
	{
		m_fTimer += deltaTime;

		static int secondsUntilBegin = 0;
		int countDown = static_cast<int>(std::ceil(m_fRoundBeginDelay - m_fTimer));
		if(countDown != secondsUntilBegin)
		{
			// Update state message
			secondsUntilBegin = countDown;
			auto pLog = m_Hud.get<Label>(m_HudStateLabel);
			pLog->setText(StateStatus[(int)m_State] + std::to_string(countDown));
		}
		
		if(m_fTimer > m_fRoundBeginDelay)
		{
			setState(State::PlayerAim);
			return;
		}
	}
	else if (m_State == State::PlayerAim)
	{
		auto pPlayer = getObject<Cannon>(m_Player);
		if(pPlayer->getState() != Cannon::State::Aiming)
		{
			setState(State::ProcessAI);
			return;
		}
	}
	else if (m_State == State::ProcessAI)
	{
		bool bProcessed = true;
		m_fTimer += deltaTime;

		if(m_fTimer > m_fAiProcessDelay)
		{
			m_fTimer = 0.0f;

			for(int i = 0; i < Settings::PlayerCount; i++)
			{
				auto pAi = getObject<CannonAi>(m_CannonAi[i]);
				if(pAi->getStatus() == CannonAi::Status::Processing)
				{
					bProcessed = false;
					pAi->update(deltaTime);
					return;
				}
			}

			if(bProcessed)
			{
				setState(State::ShootProjectiles);
			}
		}

		return;
	}
	else if (m_State == State::ShootProjectiles)
	{
		if(getAll<Projectile>().size() == 0)
		{
			m_fTimer += deltaTime;
			if(m_fTimer > 5.0f)
			{
				if(getNumberOfPlayersAlive() > 1)
				{
					setState(State::PlayerAim);
				}
				else
				{
					setState(State::RoundOver);
				}
				return;
			}
		}	
	}
	else if (m_State == State::RoundOver)
	{
		m_fTimer += deltaTime;

		static int secondsBeforeExit = 0;
		int countDown = static_cast<int>(std::ceil(m_fRoundOverDelay - m_fTimer));
		if(countDown != secondsBeforeExit)
		{
			// Update state message
			secondsBeforeExit = countDown;
			auto pLog = m_Hud.get<Label>(m_HudStateLabel);
			pLog->setText(StateStatus[(int)m_State] + std::to_string(countDown));
		}

		if(m_fTimer > m_fRoundOverDelay)
		{
			onExitClicked();
			return;
		}
	}

	// Update objects
	auto objects = getObjects();
	const sf::FloatRect& bounds = getObject<Terrain>(m_Terrain)->getBounds();

	for(std::shared_ptr<GameObject>& obj : objects)
	{
		const sf::Vector2f& objPos = obj->getPosition();

		if(objPos.x < bounds.left - 20.0f || 
			objPos.x > bounds.left + bounds.width + 20.0f ||
			objPos.y > bounds.top + bounds.height + 20.0f)
		{
			destroyObject(obj->getId());
			continue;
		}

		obj->update(deltaTime);
	}

	// Update game systems
	Game::physics.update(deltaTime, objects);

	// Update HUD
	m_Hud.update(deltaTime);
}

void BattleScene::receive(const Message& message)
{

}

void BattleScene::draw(Renderer* pRenderer)
{
	for(std::shared_ptr<GameObject>& obj : getObjects())
	{
		pRenderer->draw(*obj);
	}

	pRenderer->draw(m_Hud);
	pRenderer->draw(m_PauseMenu);
}

void BattleScene::spawnPlayers(uint32_t iCount)
{
	// Destroy old players and their AIs
	for(uint32_t i = 0; i < Settings::MaxPlayers; i++)
	{
		if(m_Players[i] != (uint32_t) -1)
		{
			destroyObject(m_Players[i]);
			m_Players[i] = (uint32_t) -1;
		}

		if(m_CannonAi[i] != (uint32_t) -1)
		{
			destroyObject(m_CannonAi[i]);
			m_CannonAi[i] = (uint32_t) -1;
		}
	}

	std::vector<sf::Vector2f> spawnPositions;
	sf::FloatRect terrainRect = getObject<Terrain>(m_Terrain)->getBounds();
	for(uint32_t i = 0; i < iCount; i++)
	{
		float x, y;

		x = static_cast<float>(rand() % (int)(terrainRect.width - 100.0f) + 50);
		y = terrainRect.top + 20.0f;

		bool valid = true;
		for(uint32_t j = 0; j < spawnPositions.size(); j++)
		{
			if(std::abs(x - spawnPositions[j].x) < 100.0f)
			{
				valid = false;
				break;
			}
		}

		if(valid == false)
		{
			i--;
			continue;
		}

		spawnPositions.push_back(sf::Vector2f(x, y));
	}

	// Spawn new players
	for(uint32_t i = 0; i < iCount; i++)
	{
		// Create player base
		uint32_t id = spawnObject<Cannon>();
		auto pPlayer = getObject<Cannon>(id);
		{
			pPlayer->setColor(Settings::BaseColors[(i + Settings::PlayerNumber) % Settings::MaxPlayers]);

			// Random spawn position
			pPlayer->setPosition(spawnPositions[i]);
		}
		m_Players[i] = id;

		// Spawn AI
		uint32_t aiId = spawnObject<CannonAi>();
		auto pAi = getObject<CannonAi>(aiId);
		{
			pAi->setStatus(i == 0 ? CannonAi::Status::Disabled : CannonAi::Status::Ready);
			pAi->attachTo(pPlayer.get());
			pAi->setTerrain(getObject<Terrain>(m_Terrain).get());
		}
		m_CannonAi[i] = aiId;

		if(i == 0)
		{
			m_Player = id;
		}
	}

	for(int i = 0; i < Settings::PlayerCount; i++)
	{
		auto pAi = getObject<CannonAi>(m_CannonAi[i]);
		pAi->init();
	}
}

void BattleScene::setState(BattleScene::State state)
{
	// Reset timer
	m_fTimer = 0.0f;

	// Handle on exit state
	if(m_State == State::Init)
	{
	}
	else if (m_State == State::RoundBegin)
	{
	}
	else if (m_State == State::PlayerAim)
	{
	}
	else if (m_State == State::ProcessAI)
	{
	}
	else if (m_State == State::ShootProjectiles)
	{
	}
	else if (m_State == State::RoundOver)
	{
	}

	// Change state
	m_State = state;
	m_Hud.get<Label>(m_HudStateLabel)->setText(StateStatus[(int)state]);

	// Handle on enter state
	if(m_State == State::Init)
	{
	}
	else if (m_State == State::RoundBegin)
	{
	}
	else if (m_State == State::PlayerAim)
	{
		auto pPlayer = getObject<Cannon>(m_Player);
		if(pPlayer->getState() != Cannon::State::Destroyed)
		{
			pPlayer->setState(Cannon::State::Aiming);
		}

		forceMouseUpdate();
	}
	else if (m_State == State::ProcessAI)
	{
		for(int i = 0; i < Settings::PlayerCount; i++)
		{
			auto pAi = getObject<CannonAi>(m_CannonAi[i]);
			if(pAi->getStatus() != CannonAi::Status::Disabled)
			{
				pAi->setStatus(CannonAi::Status::Processing);
			}
		}
	}
	else if (m_State == State::ShootProjectiles)
	{
		for(int i = 0; i < Settings::PlayerCount; i++)
		{
			auto pPlayer = getObject<Cannon>(m_Players[i]);
			if(pPlayer->getState() != Cannon::State::Destroyed)
			{
				pPlayer->shoot();
			}
		}
	}
	else if (m_State == State::RoundOver)
	{
	}
	
}

int BattleScene::getNumberOfPlayersAlive()
{
	int count = 0;
	for(int i = 0; i < Settings::PlayerCount; i++)
	{
		auto pPlayer = getObject<Cannon>(m_Players[i]);
		if(pPlayer->getHealth() > 0.0f)
		{
			count++;
		}
	}
	return count;
}

void BattleScene::onKeyHit(sf::Keyboard::Key key)
{
	if(key == sf::Keyboard::Escape)
	{
		togglePause();
	}
}

void BattleScene::onMouseMove(sf::Vector2i pos)
{
	if(m_PauseMenu.getVisible())
		return;

	if(m_State == State::PlayerAim)
	{
		AimData data;
		data.cannon = getObject<Cannon>(m_Player).get();
		data.position = sf::Vector2f(pos.x, pos.y);

		Message msg;
		msg.id = Message::Id::Aim;
		msg.data.aim = data;

		Game::messageBus.send(msg);
	}
}

void BattleScene::onMouseUp(sf::Mouse::Button button)
{
	if(m_PauseMenu.getVisible())
		return;

	if(m_State == State::PlayerAim)
		getObject<Cannon>(m_Player)->setState(Cannon::State::Idle);
}

void BattleScene::forceMouseUpdate()
{
	sf::Window* pWindow = Game::renderer.getWindow().lock().get();
	onMouseMove(sf::Mouse::getPosition(*pWindow));
}

void BattleScene::onContinueClicked()
{
	togglePause();
}

void BattleScene::onRetryClicked()
{
	Game::sceneManager.resetScene();
}

void BattleScene::onExitClicked()
{
	Game::sceneManager.popScene();
}

void BattleScene::togglePause()
{
	if(m_PauseMenu.getVisible())
	{
		m_PauseMenu.setVisible(false);
		setInputBindings();
		setAimCursor(true);
	}
	else
	{
		m_PauseMenu.setVisible(true);
		m_PauseMenu.setInputBindings(&Game::input);

		Game::input.onKeyUp = std::bind(&BattleScene::onKeyHit, this, std::placeholders::_1);

		setAimCursor(false);
	}
}

void BattleScene::setInputBindings()
{
	Game::input.resetBindings();
	Game::input.onMouseMove = std::bind(&BattleScene::onMouseMove, this, std::placeholders::_1);
	Game::input.onMouseUp = std::bind(&BattleScene::onMouseUp, this, std::placeholders::_1);
	Game::input.onKeyUp = std::bind(&BattleScene::onKeyHit, this, std::placeholders::_1);
}

void BattleScene::setAimCursor(bool bAim)
{
	if(bAim)
	{
		sf::Texture* aim = Game::assets.getTexture("cursor_aim");
		if(aim)
		{
			Game::cursor.setTexture(*aim);
			Game::cursor.setActive(true);
			Game::cursor.setScale(2.0f, 2.0f);
		}

		Game::renderer.setShowCursor(false);
	}
	else
	{
		Game::cursor.setActive(false);
		Game::renderer.setShowCursor(true);
	}
}

void BattleScene::spawnTerrain()
{
	const sf::Vector2u& vScreenSize = Game::renderer.getScreenSize();

	m_Terrain = spawnObject<Terrain>();
	auto terrain = getObject<Terrain>(m_Terrain);

	terrain->setColor(sf::Color(0x43A047FF));
	terrain->create(vScreenSize.x, vScreenSize.y);
}

void BattleScene::initPauseMenu()
{
	sf::Font* pFont = Game::assets.getFont("default");
	if(pFont)
	{
		m_PauseMenu.setFont(pFont);
	}

	m_PauseMenu.setVisible(false);

	const sf::Vector2u& vScreenSize = Game::renderer.getScreenSize();

	sf::Vector2i center = sf::Vector2i(
		static_cast<int>(vScreenSize.x / 2.0f),
		static_cast<int>(vScreenSize.y / 2.0f));

	uint32_t panel = m_PauseMenu.createControl<Panel>();
	auto pPanel = m_PauseMenu.get<Panel>(panel);
	{
		pPanel->top = center.y - 200;
		pPanel->left = center.x - 200;
		pPanel->width = 400;
		pPanel->height = 400;
	}
	pPanel->init();

	uint32_t buttonContinue = m_PauseMenu.createControl<Button>();
	auto pButtonContinue = m_PauseMenu.get<Button>(buttonContinue);
	{
		pButtonContinue->top = center.y - 180;
		pButtonContinue->left = center.x - 180;
		pButtonContinue->width = 360;
		pButtonContinue->height = 100;
		pButtonContinue->setText("Continue");
		pButtonContinue->onClick = std::bind(&BattleScene::onContinueClicked, this);
	}
	pButtonContinue->init();

	uint32_t buttonRetry = m_PauseMenu.createControl<Button>();
	auto pButtonRetry = m_PauseMenu.get<Button>(buttonRetry);
	{
		pButtonRetry->top = center.y - 50;
		pButtonRetry->left = center.x - 180;
		pButtonRetry->width = 360;
		pButtonRetry->height = 100;
		pButtonRetry->setText("Retry");
		pButtonRetry->onClick = std::bind(&BattleScene::onRetryClicked, this);
	}
	pButtonRetry->init();

	uint32_t buttonExit = m_PauseMenu.createControl<Button>();
	auto pButtonExit = m_PauseMenu.get<Button>(buttonExit);
	{
		pButtonExit->top = center.y + 80;
		pButtonExit->left = center.x - 180;
		pButtonExit->width = 360;
		pButtonExit->height = 100;
		pButtonExit->setText("Exit");
		pButtonExit->onClick = std::bind(&BattleScene::onExitClicked, this);
	}
	pButtonExit->init();
}

void BattleScene::initHud()
{
	sf::Font* pFont = Game::assets.getFont("default");	
	if(pFont)
	{
		m_Hud.setFont(pFont);
	}

	m_Hud.setVisible(true);

	const sf::Vector2u& vScreenSize = Game::renderer.getScreenSize();

	// sf::Vector2i center = sf::Vector2i(
	// 	static_cast<int>(vScreenSize.x / 2.0f),
	// 	static_cast<int>(vScreenSize.y / 2.0f));

	uint32_t topPanel = m_Hud.createControl<Panel>();
	auto pTopPanel = m_Hud.get<Panel>(topPanel);
	{
		pTopPanel->left = 0;
		pTopPanel->top = 0;
		pTopPanel->width = (int) vScreenSize.x;
		pTopPanel->height = 48;
	}
	pTopPanel->init();

	m_HudStateLabel = m_Hud.createControl<Label>();
	auto pHudStateLabel = m_Hud.get<Label>(m_HudStateLabel);
	{
		pHudStateLabel->left = 5;
		pHudStateLabel->top = 5;
		pHudStateLabel->height = 32;
		pHudStateLabel->setText(StateStatus[(int)m_State]);
		pHudStateLabel->setInvertColors(true);
	}
	pHudStateLabel->init();

}