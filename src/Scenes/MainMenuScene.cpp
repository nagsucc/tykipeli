#include "Scenes/MainMenuScene.hpp"

#include "Game.hpp"

#include "Scenes/BattleScene.hpp"
#include "Objects/LoadIcon.hpp"

#include "GUI/Button.hpp"
#include "GUI/Panel.hpp"
#include "GUI/Label.hpp"
#include "GUI/CheckBox.hpp"
#include "GUI/ColoredCheckBox.hpp"

MainMenuScene::MainMenuScene() : Scene(SceneId::MainMenu)
{
	const sf::Vector2u& vScreenSize = Game::renderer.getScreenSize();
	sf::Vector2i center = sf::Vector2i(
		static_cast<int>(vScreenSize.x / 2.0f),
		static_cast<int>(vScreenSize.y / 2.0f));

	m_LoadIcon = spawnObject<LoadIcon>();
	auto pLoadIcon = getObject<LoadIcon>(m_LoadIcon);
	pLoadIcon->setPosition({vScreenSize.x / 2.0f, vScreenSize.y / 2.0f});
	pLoadIcon->setScale(6.0f, 6.0f);

	sf::Font* pFont = Game::assets.getFont("default");
	if(pFont)
	{
		m_Gui.setFont(pFont);
	}

	uint32_t panel = m_Gui.createControl<Panel>();
	auto pPanel = m_Gui.get<Panel>(panel);
	{
		pPanel->top = center.y - 300;
		pPanel->left = center.x - 300;
		pPanel->width = 600;
		pPanel->height = 700;
	}
	pPanel->init();

	uint32_t gameTitle = m_Gui.createControl<Label>();
	auto pGameTitle = m_Gui.get<Label>(gameTitle);
	{
		pGameTitle->top = center.y - 420;
		pGameTitle->left = center.x;
		pGameTitle->height = 72;
		pGameTitle->setAlign(Label::Align::Center);
		pGameTitle->setText(Game::sTitle);
	}
	pGameTitle->init();

	uint32_t menuTitle = m_Gui.createControl<Label>();
	auto pMenuTitle = m_Gui.get<Label>(menuTitle);
	{
		pMenuTitle->top = center.y - 290;
		pMenuTitle->left = center.x;
		pMenuTitle->height = 54;
		pMenuTitle->setAlign(Label::Align::Center);
		pMenuTitle->setText("Menu");
		pMenuTitle->setInvertColors(true);
	}
	pMenuTitle->init();

	uint32_t colorLabel = m_Gui.createControl<Label>();
	auto pColorLabel = m_Gui.get<Label>(colorLabel);
	{
		pColorLabel->top = center.y - 210;
		pColorLabel->left = center.x;
		pColorLabel->height = 32;
		pColorLabel->setAlign(Label::Align::Center);
		pColorLabel->setText("Player color");
		pColorLabel->setInvertColors(true);
	}
	pColorLabel->init();

	int checkBoxWidth = static_cast<int>((400.0f - 4.0f * (Settings::MaxPlayers - 1)) / Settings::MaxPlayers);
	for(int i = 0; i < Settings::MaxPlayers; i++)
	{
		uint32_t checkBox = m_Gui.createControl<ColoredCheckBox>();
		auto pCb = m_Gui.get<ColoredCheckBox>(checkBox);
		{
			pCb->top = center.y - 150;
			pCb->left = center.x - 200 + (4 * i + i * checkBoxWidth);
			pCb->width = checkBoxWidth;
			pCb->height = checkBoxWidth;
			pCb->setChecked(i == Settings::PlayerNumber);
			pCb->onValueChanged = std::bind(&MainMenuScene::onSelectColor, this, i);
			pCb->setBoxColor(Settings::BaseColors[i]);
		}
		m_ColorToggles[i] = checkBox;
		pCb->init();
	}

	uint32_t playerCountLabel = m_Gui.createControl<Label>();
	auto pPlayerCountLabel = m_Gui.get<Label>(playerCountLabel);
	{
		pPlayerCountLabel->top = center.y - 50;
		pPlayerCountLabel->left = center.x;
		pPlayerCountLabel->height = 32;
		pPlayerCountLabel->setAlign(Label::Align::Center);
		pPlayerCountLabel->setText("Number of opponents");
		pPlayerCountLabel->setInvertColors(true);
	}
	pPlayerCountLabel->init();

	int checkBoxWidth2 = static_cast<int>(400.0f / (Settings::MaxPlayers - Settings::MinPlayers + 1));
	for(int i = 0; i <= Settings::MaxPlayers - Settings::MinPlayers; i++)
	{
		uint32_t checkBox = m_Gui.createControl<CheckBox>();
		auto pCb = m_Gui.get<CheckBox>(checkBox);
		{
			pCb->top = center.y + 10;
			pCb->left = center.x - 200 + i * checkBoxWidth2;
			pCb->width = checkBoxWidth;
			pCb->height = checkBoxWidth;
			pCb->setText(std::to_string(i + Settings::MinPlayers - 1));
			pCb->setChecked(i + Settings::MinPlayers == Settings::PlayerCount);
			pCb->onValueChanged = std::bind(&MainMenuScene::onSelectPlayerCount, this, i + Settings::MinPlayers);
		}
		m_OpponentCountToggles[i] = checkBox;
		pCb->init();
	}

	uint32_t buttonStart = m_Gui.createControl<Button>();
	auto btnStart = m_Gui.get<Button>(buttonStart);
	{
		btnStart->top = center.y + 120;
		btnStart->left = center.x - 200;
		btnStart->width = 400;
		btnStart->height = 100;
		btnStart->setText("Begin");
		btnStart->onClick = std::bind(&MainMenuScene::onBeginClicked, this);
	}
	btnStart->init();

	uint32_t buttonExit = m_Gui.createControl<Button>();
	auto btnExit = m_Gui.get<Button>(buttonExit);
	{
		btnExit->top = center.y + 250;
		btnExit->left = center.x - 200;
		btnExit->width = 400;
		btnExit->height = 100;
		btnExit->setText("Exit");
		btnExit->onClick = std::bind(&MainMenuScene::onExitClicked, this);
	}
	btnExit->init();
}

MainMenuScene::~MainMenuScene()
{

}

void MainMenuScene::onEnter()
{
	Game::renderer.setWindowColor(sf::Color::Black);
	Game::renderer.setShowCursor(true);
	m_Gui.setInputBindings(&Game::input);
}

void MainMenuScene::onExit()
{
	m_Gui.setInputBindings(nullptr);
}

void MainMenuScene::update(const float& deltaTime)
{
	getObject<LoadIcon>(m_LoadIcon)->update(deltaTime);
	m_Gui.update(deltaTime);
}

void MainMenuScene::receive(const Message& message)
{
}

void MainMenuScene::draw(Renderer* pRenderer)
{
	pRenderer->draw(*getObject<LoadIcon>(m_LoadIcon).get());
	pRenderer->draw(m_Gui);
}

void MainMenuScene::onBeginClicked()
{
	Game::sceneManager.pushScene(SceneId::Battle);
}

void MainMenuScene::onExitClicked()
{
	Message msg;
	msg.id = Message::Id::Exit;
	Game::messageBus.send(msg);
}

void MainMenuScene::onSelectColor(uint32_t index)
{
	m_Gui.get<CheckBox>(m_ColorToggles[Settings::PlayerNumber])->uncheck();
	Settings::PlayerNumber = index;
	m_Gui.get<CheckBox>(m_ColorToggles[Settings::PlayerNumber])->check();
}

void MainMenuScene::onSelectPlayerCount(uint32_t count)
{
	m_Gui.get<CheckBox>(m_OpponentCountToggles[Settings::PlayerCount - Settings::MinPlayers])->uncheck();
	Settings::PlayerCount = count;
	m_Gui.get<CheckBox>(m_OpponentCountToggles[Settings::PlayerCount - Settings::MinPlayers])->check();
}