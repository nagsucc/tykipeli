#include "GameObject.hpp"

GameObject::GameObject()
{

}

GameObject::~GameObject()
{

}

uint32_t GameObject::getId() const
{
	return m_Id;
}

void GameObject::setActive(bool bActive)
{
	m_bActive = bActive;
}

bool GameObject::isActive() const
{
	return m_bActive;
}

Scene* GameObject::getScene() const
{
	return m_pScene;
}

void GameObject::setVelocity(const sf::Vector2f& velocity)
{
	m_velocity = velocity;
}

const sf::Vector2f& GameObject::getVelocity() const
{
	return m_velocity;
}

void GameObject::setMass(float fMass)
{
	m_fMass = fMass;
}

float GameObject::getMass() const
{
	return m_fMass;
}

void GameObject::enablePhysics(bool bEnable)
{
	m_bIgnorePhysics = !bEnable;
}

bool GameObject::isPhysicsEnabled() const
{
	return !m_bIgnorePhysics;
}