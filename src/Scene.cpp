#include "Scene.hpp"
#include "Game.hpp"

Scene::Scene(SceneId id) : m_Id(id)
{

}

Scene::~Scene()
{

}

SceneId Scene::getId() const
{
	return m_Id;
}

void Scene::onEnter()
{
	m_bActive = true;
}

void Scene::onExit()
{
	m_bActive = false;
}

void Scene::update(const float& deltaTime)
{

}

void Scene::receive(const Message& message)
{

}

void Scene::draw(Renderer* pRenderer)
{

}

bool Scene::isActive() const
{
	return m_bActive;
}

void Scene::setActive(bool bActive)
{
	m_bActive = bActive;
}

void Scene::destroyObject(uint32_t iObjectId)
{
	auto it = m_Objects.find(iObjectId);
	if(it != m_Objects.end())
	{
		auto pObj = it->second;
		for(size_t i = 0; i < m_ObjectPointers.size(); i++)
		{
			if(m_ObjectPointers[i] == pObj)
			{
				m_ObjectPointers.erase(m_ObjectPointers.begin() + i);
				break;
			}
		}

		m_Objects.erase(it);

		Game::log.note("[Scene] destroyObject " + std::to_string(iObjectId));
	}
}

std::vector<std::shared_ptr<GameObject>> Scene::getObjects() const
{
	return m_ObjectPointers;
}