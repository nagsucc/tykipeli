#include "Physics.hpp"

Physics::Physics()
{

}

Physics::~Physics()
{

}

void Physics::setGravity(float fGravity)
{
	m_fGravity = fGravity;
}

float Physics::getGravity() const
{
	return m_fGravity;
}

void Physics::setUnit(float fPixels)
{
	m_fUnitInPixels = fPixels;
}

float Physics::getUnit() const
{
	return m_fUnitInPixels;
}

void Physics::update(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects)
{
	gravity(deltaTime, objects);
	collisions(deltaTime, objects);
	apply(deltaTime, objects);
}

void Physics::gravity(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects)
{
	for(std::shared_ptr<GameObject>& object : objects)
	{
		if(object->isPhysicsEnabled() == false)
			continue;
		
		sf::Vector2f velocity = object->getVelocity();
		velocity.y += m_fGravity * deltaTime;
		object->setVelocity(velocity);
	}
}

void Physics::collisions(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects)
{
	unsigned int i = 0;
	for(std::shared_ptr<GameObject>& object : objects)
	{
		unsigned int j = i + 1;
		for(; j < objects.size(); j++)
		{
			object->checkCollision(objects[j].get());
		}
		i++;
	}
}

void Physics::apply(const float& deltaTime, std::vector<std::shared_ptr<GameObject>> objects)
{
	for(std::shared_ptr<GameObject>& object : objects)
	{
		sf::Vector2f velocity = object->getVelocity();
		object->move(velocity * deltaTime * m_fUnitInPixels);
	}
}
