#include "Assets.hpp"
#include "Game.hpp"

Assets::Assets()
{

}

Assets::~Assets()
{
	m_Textures.clear();
	m_Fonts.clear();
	m_Sounds.clear();
	m_Strings.clear();
}


bool Assets::loadTexture(const std::string& filename, const std::string& id)
{
	sf::Texture texture;
	bool result = texture.loadFromFile(filename);
	
	if(result == true)
	{
		std::string id_ = id;

		if(id_.empty())
			id_ = parseFilename(filename);

		m_Textures[id_] = texture;
	}
	else
	{
		Game::log.error("[Assets] Missing texture " + filename + " (" + id + ")");
	}
	
	return result;
}

bool Assets::loadFont(const std::string& filename, const std::string& id)
{
	sf::Font font;
	bool result = font.loadFromFile(filename);

	if(result == true)
	{
		std::string id_ = id;

		if(id_.empty())
			id_ = parseFilename(filename);

		m_Fonts[id_] = font;
	}
	else
	{
		Game::log.error("[Assets] Missing font " + filename + " (" + id + ")");
	}
	
	return result;
}

bool Assets::loadSound(const std::string& filename, const std::string& id)
{
	sf::SoundBuffer sound;
	bool result = sound.loadFromFile(filename);

	if(result == true)
	{
		std::string id_ = id;

		if(id_.empty())
			id_ = parseFilename(filename);

		m_Sounds[id_] = sound;
	}
	else
	{
		Game::log.error("[Assets] Missing sound " + filename + " (" + id + ")");
	}
	
	return result;
}

bool Assets::loadFileAsString(const std::string& filename, const std::string& id)
{
	bool result = false;
	sf::FileInputStream fs;

	if(fs.open(filename))
	{
		std::string str;

		long size = fs.getSize();
		str.resize(size + 1, 0);

		long read = fs.read(str.data(), size);
		result = (read == size);

		if(result)
		{
			std::string id_ = id;

			if(id_.empty())
				id_ = parseFilename(filename);

			m_Strings[id_] = str;
		}
	}

	if(result == false)
	{
		Game::log.error("[Assets] Failed to read " + filename + " (" + id + ")");
	}

	return result;
}

void Assets::releaseTexture(const std::string& id)
{
	auto it = m_Textures.find(id);
	if(it != m_Textures.end())
	{
		m_Textures.erase(it);
	}
}

void Assets::releaseFont(const std::string& id)
{
	auto it = m_Fonts.find(id);
	if(it != m_Fonts.end())
	{
		m_Fonts.erase(it);
	}
}

void Assets::releaseSound(const std::string& id)
{
	auto it = m_Sounds.find(id);
	if(it != m_Sounds.end())
	{
		m_Sounds.erase(it);
	}
}

void Assets::releaseString(const std::string& id)
{
	auto it = m_Strings.find(id);
	if(it != m_Strings.end())
	{
		m_Strings.erase(it);
	}
}

sf::Texture* Assets::getTexture(const std::string& id)
{
	auto it = m_Textures.find(id);
	if(it != m_Textures.end())
	{
		return &(it->second);
	}
	return nullptr;
}

sf::Font* Assets::getFont(const std::string& id)
{
	auto it = m_Fonts.find(id);
	if(it != m_Fonts.end())
	{
		return &(it->second);
	}
	return nullptr;
}

sf::SoundBuffer* Assets::getSound(const std::string& id)
{
	auto it = m_Sounds.find(id);
	if(it != m_Sounds.end())
	{
		return &(it->second);
	}
	return nullptr;
}

std::string Assets::getString(const std::string& id)
{
	auto it = m_Strings.find(id);
	if(it != m_Strings.end())
	{
		return it->second;
	}
	return id;
}

std::string Assets::parseFilename(const std::string& filename)
{
	std::string fn = filename;

	size_t begin = fn.find_last_of('/');
	if(begin != std::string::npos)
	{
		fn = fn.substr(begin + 1);
	}

	size_t end = fn.find_last_of('.');
	if(end != std::string::npos)
	{
		fn = fn.substr(0, end);
	}
	
	return fn;
}