#include "Listener.hpp"
#include <sstream>
#include "Game.hpp"

Listener::Listener(const std::string& name)
{
	if(name.empty())
	{
		std::stringstream ss;
		ss << this;
		m_sName = ss.str();
	}
	else
	{
		m_sName = name;
	}

	Game::messageBus.registerListener(this);
}

Listener::~Listener()
{
	Game::messageBus.unregisterListener(this);
}

const std::string& Listener::getName() const
{
	return m_sName;
}
