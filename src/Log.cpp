#include "Log.hpp"
#include <iomanip>
#include <sstream>

Log::Log()
{

}

Log::~Log()
{

}

void Log::setOutputStream(std::ostream* pStream)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_streamOut = pStream;
}

void Log::useConsoleColors(bool bEnabled)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	m_bUseConsoleColors = bEnabled;
}

void Log::setSuppressMessages(bool bSuppress, Message::Id id)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if(id == Message::Id::Count)
	{
		m_bSuppressMessages = bSuppress;
	}
	else
	{
		m_bSuppressMessageIds[(int)id] = bSuppress;
	}
}

void Log::receive(const Message& message)
{
	if(m_bSuppressMessages) return;
	if(m_bSuppressMessageIds[(int)message.id]) return;

	std::stringstream outString;
	outString << " [RECV MESSAGE] " << Message::IdName[(int)message.id];

	Log::info(outString.str());
}

void Log::info(const std::string& message)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string msg = message;
	setColor_(m_ColorInfo, msg);
	log_(msg);
}

void Log::note(const std::string& message)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string msg = message;
	setColor_(m_ColorNote, msg);
	log_(msg);
}

void Log::success(const std::string& message)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string msg = message;
	setColor_(m_ColorSuccess, msg);
	log_(msg);
}

void Log::warning(const std::string& message)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string msg = message;
	setColor_(m_ColorWarning, msg);
	log_(msg);
}

void Log::error(const std::string& message)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string msg = message;
	setColor_(m_ColorError, msg);
	log_(msg);
}

void Log::setColor_(Color color, std::string& message)
{
	std::string colorCode;

	switch(color)
	{
		default:
		case Color::Default: colorCode = "\033[0m"; break;
		case Color::Gray: colorCode = "\033[37m"; break;
		case Color::Red: colorCode = "\033[91m"; break;
		case Color::Blue: colorCode = "\033[34m"; break;
		case Color::Green: colorCode = "\033[32m"; break;
		case Color::Yellow: colorCode = "\033[93m"; break;

	}

	message = colorCode + message + "\033[0m";
}

void Log::log_(const std::string& message)
{
	time_t t;
	struct tm * timeinfo;

	time( &t );
	timeinfo = localtime(&t);

	*m_streamOut 
		<< std::setfill('0')
		<< "[ " 
		<< (timeinfo->tm_year + 1900) << "-" 
		<< std::setw(2) << (timeinfo->tm_mon + 1) << "-"
		<< std::setw(2) << (timeinfo->tm_mday) << " " 
		<< std::setw(2) << (timeinfo->tm_hour) << ":" 
		<< std::setw(2) << (timeinfo->tm_min) << ":" 
		<< std::setw(2) << (timeinfo->tm_sec)
		<< " ] " 
		<< message 
		<< '\n'
		<< std::setfill(' ');
}