#include "Input.hpp"

Input::Input()
{

}

Input::~Input()
{

}

void Input::receive(const Message& message)
{
	if(message.id != Message::Id::Event)
	{
		return;
	}

	sf::Event event = message.data.event;
	switch(event.type)
	{
		case sf::Event::KeyPressed:	
			if(m_bTextInput == false && onKeyDown)
			{
				onKeyDown(event.key.code);
			}
			break;
		case sf::Event::KeyReleased: 
			if(m_bTextInput == false && onKeyUp)
			{
				onKeyUp(event.key.code);
			}
			break;

		case sf::Event::TextEntered:
			if(m_bTextInput == true && onTextEntered)
			{
				onTextEntered(event.text.unicode);
			}
			break;

		case sf::Event::MouseButtonPressed:
			if(onMouseDown)
				onMouseDown(event.mouseButton.button); 
			break;

		case sf::Event::MouseButtonReleased: 
			if(onMouseUp)
				onMouseUp(event.mouseButton.button); 
			break;

		case sf::Event::MouseWheelScrolled:
			if(onMouseScroll)
				onMouseScroll(event.mouseWheelScroll.wheel, event.mouseWheelScroll.delta);
			break;

		case sf::Event::MouseMoved:
			if(onMouseMove)
				onMouseMove(sf::Vector2i(event.mouseMove.x, event.mouseMove.y));
			break;

		case sf::Event::JoystickConnected:
			if(onControllerConnected)
				onControllerConnected(event.joystickConnect.joystickId);
			break;

		case sf::Event::JoystickDisconnected:
			if(onControllerDisconnected)
				onControllerDisconnected(event.joystickConnect.joystickId);
			break;

		case sf::Event::JoystickMoved:
			if(onControllerMove)
				onControllerMove(
					event.joystickMove.axis, 
					event.joystickMove.joystickId, 
					event.joystickMove.position);
			break;

		case sf::Event::JoystickButtonPressed:
			if(onControllerButtonDown)
				onControllerButtonDown(
					event.joystickButton.joystickId, 
					event.joystickButton.button);
			break;

		case sf::Event::JoystickButtonReleased:
			if(onControllerButtonUp)
				onControllerButtonUp(
					event.joystickButton.joystickId,
					event.joystickButton.button);
			break;

		default: 
			break;
	}
}

void Input::resetBindings()
{
	resetBinding(onKeyDown);
	resetBinding(onKeyUp);
	resetBinding(onMouseDown);
	resetBinding(onMouseUp);
	resetBinding(onMouseMove);
	resetBinding(onMouseScroll);
	resetBinding(onControllerConnected);
	resetBinding(onControllerDisconnected);
	resetBinding(onControllerButtonDown);
	resetBinding(onControllerButtonUp);
	resetBinding(onControllerMove);
	resetBinding(onTextEntered);
}

bool Input::isKeyDown(sf::Keyboard::Key key)
{
	return sf::Keyboard::isKeyPressed(key);
}

bool Input::isMouseDown(sf::Mouse::Button button)
{
	return sf::Mouse::isButtonPressed(button);
}

bool Input::isControllerButtonDown(unsigned int controllerId, unsigned int button)
{
	return sf::Joystick::isButtonPressed(controllerId, button);
}

bool Input::isControllerConnected(unsigned int controllerId)
{
	return sf::Joystick::isConnected(controllerId);
}

void Input::setTextInputMode(bool bEnable)
{
	m_bTextInput = bEnable;
}

bool Input::getTextInputMode() const
{
	return m_bTextInput;
}