#include "Renderer.hpp"
#include "Game.hpp"

Renderer::Renderer()
{

}

Renderer::~Renderer()
{

}

void Renderer::init()
{
	m_pWindow = std::make_shared<sf::RenderWindow>(
		this->m_VideoMode,
		Game::sTitle,
		this->m_iStyle
	);

	m_pWindow->setMouseCursorVisible(this->m_bShowCursor);
	m_pWindow->setFramerateLimit(this->m_iFrameLimit);

	if(Game::sIcon.empty() == false)
	{
		sf::Image icon;
		if(icon.loadFromFile("res/" + Game::sIcon))
		{
			const sf::Vector2u& iconSize = icon.getSize();
			m_pWindow->setIcon(iconSize.x, iconSize.y, icon.getPixelsPtr());
		}
	}
}

void Renderer::uninit()
{
	m_pWindow->close();
}

void Renderer::update(const float& deltaTime)
{
	this->m_pWindow->setTitle(Game::sTitle + " | " + std::to_string(Game::timer.getSmoothFps()) + " fps");
}

void Renderer::receive(const Message& message)
{
	if(message.id == Message::Id::Event)
	{
		const sf::Event& event = message.data.event;
		if(event.type == sf::Event::Closed)
		{
			m_pWindow->close();
		}
	}
	else if (message.id == Message::Id::Exit)
	{
		m_pWindow->close();
	}
}

void Renderer::beginDraw()
{
	m_pWindow->clear(this->m_WindowColor);
}

void Renderer::draw(const std::vector<sf::Drawable*>& drawables)
{
	for(sf::Drawable* pDrawable : drawables)
	{
		m_pWindow->draw(*pDrawable);
	}
}

void Renderer::draw(const sf::Drawable& drawable)
{
	m_pWindow->draw(drawable);
}

void Renderer::endDraw()
{
	m_pWindow->display();
}

std::weak_ptr<sf::RenderWindow> Renderer::getWindow() const
{
	return this->m_pWindow;
}

sf::Vector2u Renderer::getScreenSize() const
{
	return m_pWindow->getSize();
}

void Renderer::setScreenSize(const sf::Vector2u& vSize)
{
	m_pWindow->setSize(vSize);
}

uint32_t Renderer::getFramelimit() const
{
	return m_iFrameLimit;
}

void Renderer::setFramelimit(uint32_t iLimit)
{
	m_iFrameLimit = iLimit;
	if(m_pWindow)
	{
		m_pWindow->setFramerateLimit(m_iFrameLimit);
	}
}

sf::Color Renderer::getWindowColor() const
{
	return m_WindowColor;
}

void Renderer::setWindowColor(sf::Color color)
{
	m_WindowColor = color;
}

void Renderer::setShowCursor(bool bShow)
{
	m_bShowCursor = bShow;
	m_pWindow->setMouseCursorVisible(bShow);
}