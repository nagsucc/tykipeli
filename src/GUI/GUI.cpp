#include "GUI/GUI.hpp"
#include <SFML/Graphics.hpp>

GUI::GUI()
{

}

GUI::~GUI()
{

}

void GUI::setInputBindings(Input* pInputSystem)
{
	if(m_pInput != nullptr)
	{
		m_pInput->resetBindings();
	}

	m_pInput = pInputSystem;

	if(m_pInput != nullptr)
	{
		m_pInput->resetBindings();
		m_pInput->onMouseDown = std::bind(&GUI::onMouseDown, this, std::placeholders::_1);
		m_pInput->onMouseUp = std::bind(&GUI::onMouseUp, this, std::placeholders::_1);
		m_pInput->onMouseMove = std::bind(&GUI::onMouseMove, this, std::placeholders::_1);
		m_pInput->onMouseScroll = std::bind(&GUI::onMouseScroll, this, std::placeholders::_1, std::placeholders::_2);
		m_pInput->onTextEntered = std::bind(&GUI::onTextEntered, this, std::placeholders::_1);
	}
}

void GUI::update(const float& deltaTime)
{
	for(auto& control : m_Controls)
	{
		control.second->update(deltaTime);
	}
}

void GUI::setVisible(bool bVisible)
{
	m_bVisible = bVisible;
}

bool GUI::getVisible() const
{
	return m_bVisible;
}

void GUI::destroyControl(uint32_t id)
{
	auto it = m_Controls.find(id);
	if(it != m_Controls.end())
	{
		m_Controls.erase(it);
	}
}

void GUI::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->m_bVisible == false)
		return;
		
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		if(pair.second->isVisible())
		{
			target.draw(*(pair.second.get()), states);
		}
	}
}

void GUI::onMouseMove(sf::Vector2i pos)
{
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		auto pControl = pair.second;

		if(pControl->isEnabled() == false)
		{
			continue;
		}

		if(pControl->contains(pos))
		{
			if(pControl->isHovering() == false)
			{
				pControl->onEnter_();
			}
		}
		else
		{
			if(pControl->isDragged() == false && pControl->isHovering())
			{
				pControl->onExit_();
			}
		}

		if(pControl->isDragged())
		{
			pControl->onDrag_(pos);
		}
	}
}

void GUI::onMouseDown(sf::Mouse::Button button)
{
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		auto pControl = pair.second;

		if(pControl->isEnabled() == false)
		{
			continue;
		}

		if(pControl->isHovering())
		{
			pControl->onDragBegin_();
		}
	}
}

void GUI::onMouseUp(sf::Mouse::Button button)
{
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		auto pControl = pair.second;

		if(pControl->isEnabled() == false)
		{
			continue;
		}

		if(pControl->isDragged())
		{
			pControl->onDragEnd_();
		}
		else if (pControl->isHovering())
		{
			pControl->onClick_(button);
		}
	}
}

void GUI::onMouseScroll(sf::Mouse::Wheel wheel, int delta)
{
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		auto pControl = pair.second;

		if(pControl->isEnabled() == false)
		{
			continue;
		}

		if(pControl->isHovering())
		{
			pControl->onScroll_(wheel, delta);
		}
	}
}

void GUI::onTextEntered(uint32_t unicode)
{
	auto controls = m_Controls;
	for(auto& pair : controls)
	{
		auto pControl = pair.second;

		if(pControl->isEnabled() == false)
		{
			continue;
		}

		if(pControl->isActive())
		{
			pControl->onTextEntered(unicode);
		}
	}
}

void GUI::setColors(const GUI::Colors& colors)
{
	m_Colors = colors;
}

const GUI::Colors& GUI::getColors() const
{
	return m_Colors;
}

void GUI::setFont(sf::Font* font)
{
	this->m_pFont = font;
}

sf::Font* GUI::getFont() const
{
	return this->m_pFont;
}

void GUI::setBorderWidth(float fWidth)
{
	m_fBorderWidth = fWidth;
}

float GUI::getBorderWidth() const
{
	return m_fBorderWidth;
}