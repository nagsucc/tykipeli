#include "GUI/ColoredCheckBox.hpp"

ColoredCheckBox::ColoredCheckBox()
{

}

ColoredCheckBox::~ColoredCheckBox()
{

}

void ColoredCheckBox::setBoxColor(const sf::Color& color)
{
	m_Color = color;
}

void ColoredCheckBox::setColor(const sf::Color& background, const sf::Color& foreground)
{
	CheckBox::setColor(background, foreground);
	m_Box.setFillColor(m_Color);
}