#include "GUI/CheckBox.hpp"
#include "GUI/GUI.hpp"

CheckBox::CheckBox()
{

}

CheckBox::~CheckBox()
{

}

void CheckBox::init()
{
	sf::Font* font = m_pParent->getFont();
	m_Label.setFont(*font);

	apply();
	onEnable_();
}

void CheckBox::uninit()
{

}

void CheckBox::update(const float& deltaTime)
{
	Control::update(deltaTime);
}

void CheckBox::apply()
{
	if(m_bEnabled) onEnable_();
	else onDisable_();

	m_Box.setPosition(left, top);
	m_Box.setSize(sf::Vector2f(height, height));

	m_Mark.setPosition(left, top);
	m_Mark.setOrigin(height / -4.0f, height / -4.0f);
	m_Mark.setSize(sf::Vector2f(height / 2.0f, height / 2.0f));

	m_Label.setPosition(sf::Vector2f(left + height * 1.25f, top));
	setText(m_Label.getString());
}

bool CheckBox::isChecked() const
{
	return m_bChecked;
}

void CheckBox::setChecked(bool bChecked)
{
	m_bChecked = bChecked;
}

void CheckBox::check()
{
	m_bChecked = true;
}

void CheckBox::uncheck()
{
	m_bChecked = false;
}

void CheckBox::setText(const std::string& text)
{
	m_Label.setString(text);
	m_Label.setCharacterSize(static_cast<uint32_t>(height * 0.75));
}

void CheckBox::onEnter_()
{
	m_bHover = true;
	beginTransition(Transition::Hover, TransitionDirection::Forward);
}

void CheckBox::onExit_()
{
	m_bHover = false;
	beginTransition(Transition::Hover, TransitionDirection::Backwards);
}

void CheckBox::onClick_(sf::Mouse::Button button)
{
	m_bActive = false;
	beginTransition(Transition::Active, TransitionDirection::Backwards);

	m_bChecked = !m_bChecked;

	if(onValueChanged && m_bHover) 
		onValueChanged(m_bChecked);
}

void CheckBox::onDragBegin_()
{
	m_bActive = true;
	beginTransition(Transition::Active, TransitionDirection::Forward);
}

void CheckBox::onEnable_()
{
	m_bEnabled = true;
	beginTransition(Transition::Enable, TransitionDirection::Forward);
}

void CheckBox::onDisable_()
{
	m_bEnabled = false;
	beginTransition(Transition::Enable, TransitionDirection::Backwards);
}

void CheckBox::setColor(const sf::Color& background, const sf::Color& foreground)
{
	m_Box.setFillColor(background);
	m_Box.setOutlineColor(foreground);
	m_Mark.setFillColor(foreground);
	m_Label.setFillColor(background);
}

void CheckBox::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isVisible())
	{
		target.draw(m_Box, states);

		if(m_bChecked)
		{
			target.draw(m_Mark, states);
		}

		target.draw(m_Label, states);
	}
}