#include "GUI/Label.hpp"
#include "GUI/GUI.hpp"

Label::Label()
{

}

Label::~Label()
{

}

void Label::init()
{
	sf::Font* font = m_pParent->getFont();
	m_Text.setFont(*font);

	apply();
	onEnable_();
}

void Label::uninit()
{

}

void Label::update(const float& deltaTime)
{
	Control::update(deltaTime);
}

void Label::setAlign(Label::Align align)
{
	m_Align = align;
}

void Label::setInvertColors(bool bEnabled)
{
	m_bInvertColors = bEnabled;
}

void Label::setColor(const sf::Color& background, const sf::Color& foreground)
{
	m_Text.setFillColor(m_bInvertColors ? background : foreground);
}

void Label::onEnable_()
{
	m_bEnabled = true;
	beginTransition(Transition::Enable, TransitionDirection::Forward);
}

void Label::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isVisible())
		target.draw(m_Text, states);
}

void Label::setText(const std::string& text)
{
	m_Text.setString(text);
	m_Text.setCharacterSize(height);
}

void Label::apply()
{
	if(m_bEnabled) onEnable_();
	else onDisable_();

	sf::FloatRect bounds = m_Text.getGlobalBounds();

	if(m_Align == Align::Left)
	{
		m_Text.setPosition(left, top);
	}
	else if (m_Align == Align::Center)
	{
		m_Text.setPosition(left - bounds.width / 2.0f, top);
	}
	else if (m_Align == Align::Right)
	{
		m_Text.setPosition(left - bounds.width, top);
	}
}