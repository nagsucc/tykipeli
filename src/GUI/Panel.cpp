#include "GUI/Panel.hpp"
#include "GUI/GUI.hpp"

Panel::Panel()
{

}

Panel::~Panel()
{

}

void Panel::init()
{
	apply();
	onEnable_();
}

void Panel::uninit()
{

}

void Panel::update(const float& deltaTime)
{
	Control::update(deltaTime);
}

void Panel::setColor(const sf::Color& background, const sf::Color& foreground)
{
	m_Background.setOutlineColor(background);
	m_Background.setFillColor(foreground);
}

void Panel::onEnable_()
{
	m_bEnabled = true;
	beginTransition(Transition::Enable, TransitionDirection::Forward);
}

void Panel::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isVisible())
		target.draw(m_Background, states);
}

void Panel::apply()
{
	if(m_bEnabled) onEnable_();
	else onDisable_();

	m_Background.setPosition(left, top);
	m_Background.setSize(sf::Vector2f(width, height));
	m_Background.setOutlineThickness(m_pParent->getBorderWidth());
}