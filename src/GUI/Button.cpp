#include "GUI/Button.hpp"
#include "GUI/GUI.hpp"

Button::Button()
{

}

Button::~Button()
{

}

void Button::init()
{
	m_Border.setFillColor(sf::Color::Transparent);
	
	sf::Font* font = m_pParent->getFont();
	m_Label.setFont(*font);

	apply();
	onEnable_();
}

void Button::uninit()
{

}

void Button::update(const float& deltaTime)
{
	Control::update(deltaTime);
}

void Button::setText(const std::string& text)
{
	m_Label.setString(text);
	m_Label.setCharacterSize(height / 3);

	sf::FloatRect textRect = m_Label.getGlobalBounds();
	m_Label.setOrigin(textRect.width / 2.0f, textRect.height / 2.0f);
	m_Label.setPosition(left + width / 2.0f, top + height / 2.0f);
}

void Button::apply()
{
	if(m_bEnabled) onEnable_();
	else onDisable_();

	m_Background.setPosition(left, top);
	m_Background.setSize(sf::Vector2f(width, height));

	m_Border.setPosition(left, top);
	m_Border.setSize(sf::Vector2f(width, height));
	m_Border.setOutlineThickness(m_pParent->getBorderWidth());

	setText(m_Label.getString());	
}

void Button::onEnter_()
{
	m_bHover = true;
	beginTransition(Transition::Hover, TransitionDirection::Forward);
}

void Button::onExit_()
{
	m_bHover = false;
	beginTransition(Transition::Hover, TransitionDirection::Backwards);
}

void Button::onClick_(sf::Mouse::Button button)
{
	m_bActive = false;
	beginTransition(Transition::Active, TransitionDirection::Backwards);
	if(onClick && m_bHover) onClick();
}

void Button::onDragBegin_()
{
	m_bActive = true;
	beginTransition(Transition::Active, TransitionDirection::Forward);
}

void Button::onEnable_()
{
	m_bEnabled = true;
	beginTransition(Transition::Enable, TransitionDirection::Forward);
}

void Button::onDisable_()
{
	m_bEnabled = false;
	beginTransition(Transition::Enable, TransitionDirection::Backwards);
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(this->isVisible())
	{
		target.draw(m_Background, states);
		target.draw(m_Border, states);
		target.draw(m_Label, states);
	}
}

void Button::setColor(const sf::Color& background, const sf::Color& foreground)
{
	m_Label.setFillColor(foreground);
	m_Background.setFillColor(background);
	m_Border.setOutlineColor(foreground);
}