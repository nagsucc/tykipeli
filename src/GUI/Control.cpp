#include "GUI/Control.hpp"
#include "GUI/GUI.hpp"
#include "Utils.hpp"

Control::Control()
{

}

Control::~Control()
{

}

void Control::init()
{

}

void Control::uninit()
{

}

uint32_t Control::getId() const
{
	return m_Id;
}

void Control::update(const float& deltaTime)
{
	if(m_Transition != Transition::None)
	{
		updateTransition(deltaTime);
	}
}

void Control::apply()
{
	
}

void Control::setVisibile(bool bVisible)
{
	m_bVisible = bVisible;
}

bool Control::isVisible() const
{
	return m_bVisible;
}

void Control::setEnabled(bool bEnabled)
{
	if(bEnabled) onEnable_();
	else onDisable_();
	m_bEnabled = bEnabled;
}

bool Control::isEnabled() const
{
	return m_bEnabled;
}

bool Control::isDragged() const
{
	return m_bDrag;
}

bool Control::isHovering() const
{
	return m_bHover;
}

bool Control::isActive() const
{
	return m_bActive;
}

void Control::beginTransition(Transition transition, TransitionDirection direction)
{
	m_Transition = transition;
	m_TransitionDirection = direction;
	m_fTransitionTimer = (direction == TransitionDirection::Forward) ? 0.0f : m_fTransitionDuration;
}

void Control::updateTransition(const float& deltaTime)
{
	if(m_TransitionDirection == TransitionDirection::Forward)
	{
		m_fTransitionTimer += deltaTime;
	}
	else
	{
		m_fTransitionTimer -= deltaTime;
	}

	float relPos = Utils::clamp(m_fTransitionTimer, 0.0f, m_fTransitionDuration) / m_fTransitionDuration;

	const GUI::Colors& colors = m_pParent->getColors();
	sf::Color background = sf::Color::Red;
	sf::Color foreground = sf::Color::Red;

	if(m_Transition == Transition::Enable)
	{
		background = Utils::interpolate(colors.BackgroundDisabled, colors.Background, relPos);
		foreground = Utils::interpolate(colors.ForegroundDisabled, colors.Foreground, relPos);
	}
	else if (m_Transition == Transition::Hover)
	{
		background = Utils::interpolate(colors.Background, colors.BackgroundHover, relPos);
		foreground = Utils::interpolate(colors.Foreground, colors.ForegroundHover, relPos);
	}
	else if (m_Transition == Transition::Active)
	{
		background = Utils::interpolate(colors.BackgroundHover, colors.BackgroundActive, relPos);
		foreground = Utils::interpolate(colors.ForegroundHover, colors.ForegroundActive, relPos);
	}

	setColor(background, foreground);

	if(m_fTransitionTimer < 0.0f || m_fTransitionTimer > m_fTransitionDuration)
	{
		m_Transition = Transition::None;
	}
}