#include "MessageBus.hpp"
#include "Listener.hpp"
#include "Game.hpp"

MessageBus::MessageBus()
{

}

MessageBus::~MessageBus()
{
	
}

void MessageBus::registerListener(Listener* pListener)
{
	//std::lock_guard<std::mutex> lock(m_Mtx);
	m_Listeners.push_back(pListener);
}

void MessageBus::unregisterListener(Listener* pListener)
{
	//std::lock_guard<std::mutex> lock(m_Mtx);

	auto it = std::find(m_Listeners.begin(), m_Listeners.end(), pListener);
	if(it != m_Listeners.end())
	{
		m_Listeners.erase(it);
	}
}

void MessageBus::send(const Message& message)
{
	// if(message.target != nullptr)
	// {
	// 	message.target->receive(message);
	// 	Game::log.receive(message);
	// }
	// else
	// {
		//std::lock_guard<std::mutex> lock(m_Mtx);
		
	for(size_t i = 0; i < m_Listeners.size(); i++)
	{
		m_Listeners[i]->receive(message);
	}
	// }
}