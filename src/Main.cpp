#include "Game.hpp"

int main()
{
    srand(time(nullptr));

    Game::sTitle = "Tykipeli";
    Game::sVersion = "0.1";
    //Game::renderer.setFramelimit(0);

    Game::init();
    
    Game::log.setSuppressMessages(true);

    Game::sceneManager.pushScene(SceneId::MainMenu);

    Game::run();
    Game::uninit();

    return 0;
}