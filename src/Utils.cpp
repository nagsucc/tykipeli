#include "Utils.hpp"
#include <cmath>
#include "Game.hpp"

uint32_t Utils::wavelength2ARGB(float wavelength)
{
	float r, g, b;
	
    if (wavelength >= 380.0f && wavelength < 440.0f)
	{
        r = -(wavelength - 440.0f) / (440.0f - 350.0f);
        g = 0.0f;
        b = 1.0f;
	}
    else if (wavelength >= 440.0f && wavelength < 490.0f)
	{   
		r = 0.0f;
        g = (wavelength - 440.0f) / (490.0f - 440.0f);
        b = 1.0f;
	}
    else if ( wavelength >= 490.0f && wavelength < 510.0f)
	{
        r = 0.0f;
        g = 1.0f;
        b = -(wavelength - 510.0f) / (510.0f - 490.0f);
	}
    else if ( wavelength >= 510.0f && wavelength < 580.0f)
	{
        r = (wavelength - 510.0f) / (580.0f - 510.0f);
        g = 1.0f;
        b = 0.0f;
	}
    else if ( wavelength >= 580.0f && wavelength < 645.0f)
	{
        r = 1.0f;
        g = -(wavelength - 645.0f) / (645.0f - 580.0f);
        b = 0.0f;
	}
    else if ( wavelength >= 645.0f && wavelength <= 780.0f)
	{
        r = 1.0f;
        g = 0.0f;
        b = 0.0f;
	}
    else
	{
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
	}

	float intensity = 0.0f;
    if (wavelength >= 380 && wavelength < 420)
	{
        intensity = 0.3f + 0.7f*(wavelength - 350) / (420 - 350);
	}
    else if(wavelength >= 420 && wavelength <= 700)
	{
        intensity = 1.0f;
	}
    else if (wavelength > 700 && wavelength <= 780)
	{
        intensity = 0.3f + 0.7f*(780 - wavelength) / (780 - 700);
    }
	else
	{
        intensity = 0.0;
	}
	
    intensity *= 255;


	uint32_t color = 0xFF + (uint32_t((r * intensity)) << 24) + (uint32_t((g * intensity)) << 16) + (uint32_t((b * intensity)) << 8);
    return color;
}

sf::Vector2f Utils::flipX(const sf::Vector2f& vec)
{
	return { static_cast<float>(Game::renderer.getScreenSize().x - vec.x), vec.y };
}

sf::Vector2f Utils::flipY(const sf::Vector2f& vec)
{
	return { vec.x, static_cast<float>(Game::renderer.getScreenSize().y - vec.y) };
}

float Utils::angle(const sf::Vector2f& a, const sf::Vector2f& b)
{
	return std::atan2((a.y - b.y), (a.x - b.x));
}

float Utils::length(const sf::Vector2f& vec)
{
	return std::sqrt(vec.x*vec.x + vec.y*vec.y);
}

float Utils::rad2deg(float rad)
{
	return static_cast<float>(rad * 180.0f / M_PI);
}

float Utils::deg2rad(float deg)
{
	return static_cast<float>(deg / (180.0f / M_PI));
}

sf::Color Utils::interpolate(const sf::Color& begin, const sf::Color& end, float amount)
{
	amount = clamp(amount, 0.0f, 1.0f);
	
	float dr, dg, db, da;
	dr = static_cast<float>(end.r - begin.r);
	dg = static_cast<float>(end.g - begin.g);
	db = static_cast<float>(end.b - begin.b);
	da = static_cast<float>(end.a - begin.a);
	
	sf::Color color;
	color.r = begin.r + static_cast<int>(dr * amount);
	color.g = begin.g + static_cast<int>(dg * amount);
	color.b = begin.b + static_cast<int>(db * amount);
	color.a = begin.a + static_cast<int>(da * amount);
		
	return color;
}

sf::Vector2f Utils::direction(float fAngle)
{
	return sf::Vector2f(std::cos(deg2rad(fAngle)), std::sin(deg2rad(fAngle)));
}
