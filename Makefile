# ===========================================
# Tykipeli makefile
# Author: Niko Junnila
# Created: 2018-08-01
# ===========================================

# Project settings
PROJECT=Tykipeli

# Compiler settings
CXX=g++
CXXFLAGS=-std=c++17
debug: CXXFLAGS += -g -Wall -DDEBUG -D_USE_MATH_DEFINES
release: CXXFLAGS += -O2 -D_USE_MATH_DEFINES

# Directories
INCLUDE_DIR=./include
LIBRARY_DIR=./lib
BUILD_DIR=./bin
debug: BUILD_DIR:=$(BUILD_DIR)/debug
release: BUILD_DIR:=$(BUILD_DIR)/release

# Libraries
LIBS=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

# -------------------------------------------

_DEPS = 
DEPS = $(patsubst %,$(INCLUDE_DIR)/%,$(_DEPS))

_OBJ = Main.o Game.o Utils.o Timer.o GameObject.o MessageBus.o Listener.o Scene.o SceneManager.o Renderer.o Log.o Message.o Physics.o Input.o Assets.o Collider.o Settings.o \
Scenes/BattleScene.o Scenes/MainMenuScene.o \
GUI/GUI.o GUI/Control.o GUI/Button.o GUI/Panel.o GUI/Label.o GUI/CheckBox.o GUI/ColoredCheckBox.o \
Objects/Cannon.o Objects/Terrain.o Objects/LoadIcon.o Objects/Cursor.o Objects/Explosion.o Objects/Projectile.o Objects/CannonAi.o

OBJ_DEBUG = $(patsubst %,$(BUILD_DIR)/debug/%,$(_OBJ))
OBJ_RELEASE = $(patsubst %,$(BUILD_DIR)/release/%,$(_OBJ))

# -------------------------------------------

.PHONY:	clean debug release prepare build

prepare: 
	mkdir -p $(BUILD_DIR) $(BUILD_DIR)/Scenes $(BUILD_DIR)/Objects $(BUILD_DIR)/GUI

$(BUILD_DIR)/debug/%.o: ./src/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -I$(INCLUDE_DIR)

$(BUILD_DIR)/release/%.o: ./src/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -I$(INCLUDE_DIR)

debug: prepare build_debug
release: prepare build_release

build_debug: $(OBJ_DEBUG)
	$(CXX) $(CXXFLAGS) -o $(BUILD_DIR)/$(PROJECT) $^ -I$(INCLUDE_DIR) $(LIBS)

build_release: $(OBJ_RELEASE)
	$(CXX) $(CXXFLAGS) -o $(BUILD_DIR)/$(PROJECT) $^ -I$(INCLUDE_DIR) $(LIBS)

clean:
	rm -rf $(BUILD_DIR)